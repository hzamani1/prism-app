﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using Mapbox.Geocoding;
using System.Globalization;
using System;
using Mapbox.Unity;
using Mapbox.Examples;
using Mapbox.Json;
using Mapbox.Utils.JsonConverters;

public class MapHandler : MonoBehaviour
{
    [Header("Mapbox Objects")]
    [Tooltip("The gameobject with the abstract map script")]
    public Transform citySimulator;
    string _searchInput = "";
    List<Feature> _features;
    ForwardGeocodeResource _resource;
    public ForwardGeocodeUserInput _searchLocation;
    bool _isSearching = false;
    [Header("UI Objects")]
    public Button load;

    public InputField longitude, latitude, postcode;
    [Header("Site & Existing building")]
    public Button delExisting;
    public int layerMask;
    List<Transform> existing;
    bool hasSetFocus = false;
    public PTAL_Viz pTAL;

    public float currentLat { get; set; }
    public float currentLon { get; set; }
    public bool hasDeleted = false;
    public bool hasMapLoaded = false;

    private bool checkIfLoaded { get; set; }
    private int prevChildCount { get; set; }

    private Vector2 latLon;

    public delegate void OnLocationChanged(float lat, float lon);
    public static event OnLocationChanged locationChanged;

    private void OnDestroy()
    {
        if (locationChanged != null)
        {
            foreach (Delegate del in locationChanged.GetInvocationList())
            {
                locationChanged -= (OnLocationChanged)del;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        load.onClick.AddListener(LoadMap);
        delExisting.onClick.AddListener(DeleteExisting);
        _searchLocation.OnGeocoderResponse += SearchLocation_OnGeocoderResponse;
        longitude.text = (-0.098274).ToString();
        latitude.text = (51.512482).ToString();
        layerMask = 1 << 31;
        existing = new List<Transform>();
        _resource = new ForwardGeocodeResource("");
    }


    // Update is called once per frame
    void Update()
    {
        if (checkIfLoaded)
        {
            int counter = 0;
            foreach (Transform tile in citySimulator)
            {
                foreach (Transform b in tile)
                {
                    if (Physics.Raycast(b.GetComponent<MeshRenderer>().bounds.center, -Vector3.up, Mathf.Infinity, layerMask))
                    {
                        counter++;
                    }
                }
            }
            if (prevChildCount == counter)
            {
                DeleteExisting();
            }
            else
            {
                prevChildCount = counter;
            }
        }
    }

    public void SetLatLon(float lat, float lon)
    {
        latLon = new Vector2((float)lat, (float)lon);
        latitude.text = latLon.x.ToString();
        longitude.text = latLon.y.ToString();
        LoadMap();
        currentLat = latLon.x;
        currentLon = latLon.y;
        if (locationChanged != null)
        {
            locationChanged(latLon.x, latLon.y);
        }
    }

    public void LoadMap()
    {
        hasMapLoaded = true;
        pTAL.LoadMap(false);
        if (latitude.text != null && longitude.text != null)
        {
            Vector2d latlong = new Vector2d(float.Parse(latitude.text.Replace(" ", string.Empty)), float.Parse(longitude.text.Replace(" ", string.Empty)));
            currentLat = (float)latlong.x;
            currentLon = (float)latlong.y;
            citySimulator.GetComponent<AbstractMap>().SetCenterLatitudeLongitude(latlong);
        }

        if (citySimulator.childCount == 0)
        {
            citySimulator.GetComponent<AbstractMap>().SetUpMap();
        }
        else
        {
            Vector2d latlong = new Vector2d(float.Parse(latitude.text), float.Parse(longitude.text));
            citySimulator.GetComponent<AbstractMap>().UpdateMap(latlong, citySimulator.GetComponent<AbstractMap>().Zoom);
        }
        hasMapLoaded = true;
    }

    public void DeleteExisting()
    {
        int counter = 0;
        foreach (Transform tile in citySimulator)
        {
            foreach (Transform b in tile)
            {
                if (Physics.Raycast(b.GetComponent<MeshRenderer>().bounds.center, -Vector3.up, Mathf.Infinity, layerMask))
                {
                    existing.Add(b);
                    b.gameObject.SetActive(false);
                    counter++;
                }
            }
        }

        if (counter == 0)
        {
            checkIfLoaded = true;
        }
        else
        {
            hasDeleted = true;
            checkIfLoaded = false;
        }
    }

    public void DeleteAfterLoad()
    {
        foreach (Transform tile in citySimulator)
        {

            foreach (Transform b in tile)
            {
                if (Physics.Raycast(b.GetComponent<MeshRenderer>().bounds.center, -Vector3.up, Mathf.Infinity, layerMask))
                {
                    existing.Add(b);
                    b.gameObject.SetActive(false);
                }
            }
        }
        hasDeleted = true;
        checkIfLoaded = false;
    }

    #region LOAD MAP WITH POSTCODE
    void SearchLocation_OnGeocoderResponse(ForwardGeocodeResponse response)
    {
        latLon = new Vector2((float)_searchLocation.Coordinate.x, (float)_searchLocation.Coordinate.y);
        latitude.text = latLon.x.ToString();
        longitude.text = latLon.y.ToString();
        LoadMap();
        currentLat = latLon.x;
        currentLon = latLon.y;
        if (locationChanged != null)
        {
            locationChanged(latLon.x, latLon.y);
        }
    }


    /*
    void OnPostcode()
    {
        print("lala");
        _searchInput = postcode.text;
        string oldSearchInput = _searchInput;

        bool changed = oldSearchInput != _searchInput;
        if (changed)
        {
            HandleUserInput(_searchInput);
        }

        if (_features.Count > 0)
        {
            
            for (int i = 0; i < _features.Count; i++)
            {
                Feature feature = _features[i];
                string coordinates = feature.Center.x.ToString(CultureInfo.InvariantCulture) + ", " +
                                            feature.Center.y.ToString(CultureInfo.InvariantCulture);

                //abreviated coords for display in the UI
                string truncatedCoordinates = feature.Center.x.ToString("F2", CultureInfo.InvariantCulture) + ", " +
                    feature.Center.y.ToString("F2", CultureInfo.InvariantCulture);

                //split feature name and add elements until the maxButtonContentLenght is exceeded
                string[] featureNameSplit = feature.PlaceName.Split(',');
                string buttonContent = "";
                int maxButtonContentLength = 30;
                for (int j = 0; j < featureNameSplit.Length; j++)
                {
                    if (buttonContent.Length + featureNameSplit[j].Length < maxButtonContentLength)
                    {
                        if (String.IsNullOrEmpty(buttonContent))
                        {
                            buttonContent = featureNameSplit[j];
                        }
                        else
                        {
                            buttonContent = buttonContent + "," + featureNameSplit[j];
                        }
                    }
                }

                if (buttonContent.Length < maxButtonContentLength + 15)
                {
                    buttonContent = buttonContent + "," + " (" + truncatedCoordinates + ")";
                }


                if (GUILayout.Button(buttonContent))
                {
                    postcode.text = coordinates;
                    //postcode.serializedObject.ApplyModifiedProperties();
                    //EditorUtility.SetDirty(postcode.serializedObject.targetObject);

                    //Close();
                }
            }
        }
        else
        {
            if (_isSearching)
                GUILayout.Label("Searching...");
            else
                GUILayout.Label("No search results");
        }


        if (!hasSetFocus)
        {
            //GUI.FocusControl(searchFieldName);
            hasSetFocus = true;
        }
    }

    void HandleUserInput(string searchString)
    {
        _features = new List<Feature>();
        _isSearching = true;

        if (!string.IsNullOrEmpty(searchString))
        {
            _resource.Query = searchString;
            MapboxAccess.Instance.Geocoder.Geocode(_resource, HandleGeocoderResponse);
        }
    }

    void HandleGeocoderResponse(ForwardGeocodeResponse res)
    {
        //null if no internet connection
        if (res != null)
        {
            //null if invalid token
            if (res.Features != null)
            {
                _features = res.Features;
            }
        }
        _isSearching = false;


        //_hasResponse = true;
        //_coordinate = res.Features[0].Center;
        //Response = res;
        //if (OnGeocoderResponse != null)
        //{
        //	OnGeocoderResponse(this, EventArgs.Empty);
        //}
    }
    */
    #endregion
}
