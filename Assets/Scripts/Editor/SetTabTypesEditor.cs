﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BrydenWoodUnity.UIElements
{
    [CustomEditor(typeof(SetTabType))]
    [CanEditMultipleObjects]
    public class SetTabTypesEditor : Editor
    {

        private int index { get; set; }

        private void OnEnable()
        {
            SetTabType setType = (SetTabType)target;
            index = (int)setType.tabType;
        }


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            SetTabType setType = (SetTabType)target;
            //myTarget.tabType = (TabTypes)EditorGUILayout.EnumPopup("Select Tab Type", myTarget.tabType);

            if (index != (int)setType.tabType)
            {
                setType.TabSelecttor((int)setType.tabType);
                index = (int)setType.tabType;
            }

        }

    }
}
