﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Lighting;
using BrydenWoodUnity.UIElements;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for buildings
    /// </summary>
    public class ProceduralBuilding : MonoBehaviour
    {
        #region Public Properties
        [Header("Prefabs:")]
        public GameObject proceduralFloorPrefab;
        public GameObject proceduralTowerPrefab;

        [Header("Scene References:")]
        public ProceduralBuildingManager buildingManager;
        public BuildingElement buildingElement;

        [Header("Settings:")]
        public BuildingType buildingType = BuildingType.Linear;
        public PreviewMode previewMode = PreviewMode.Buildings;
        public CoreAllignment coreAllignment = CoreAllignment.Centre;
        public LinearTypology typology = LinearTypology.Single;
        public float extraFloorDepth;
        public bool useCanvas = true;
        public float distanceOffset = 1.0f;
        public float lineWidth = 0.5f;
        public float offsetDistanceLine = 4;
        public float totalHeight;
        public string numbersFormat { get { return buildingManager.numbersFormat; } }

        public int maxLevel
        {
            get
            {
                int levels = 0;

                for (int i = 0; i < floors.Count; i++)
                {
                    if (levels < floors[i].levels.Last())
                        levels = floors[i].levels.Last();
                }

                return levels;
            }
        }

        private Vector3 _coreDimensions;
        public Vector3 currentCoreDimensions
        {
            get
            {
                Vector3 _dims = new Vector3(8, 3, 8);
                float structuralWall = 0.3f;
                var lvls = maxLevel;
                if (lvls < Standards.TaggedObject.towerHeightsForCores[0])
                {
                    _coreDimensions = Standards.TaggedObject.coreDimensions[0];
                    structuralWall = Standards.TaggedObject.structuralWallDimensions[0];
                }
                else if (lvls >= Standards.TaggedObject.towerHeightsForCores[0] && lvls < Standards.TaggedObject.towerHeightsForCores[1])
                {
                    _coreDimensions = Standards.TaggedObject.coreDimensions[1];
                    structuralWall = Standards.TaggedObject.structuralWallDimensions[1];
                }
                else
                {
                    _coreDimensions = Standards.TaggedObject.coreDimensions[2];
                    structuralWall = Standards.TaggedObject.structuralWallDimensions[2];
                }
                Standards.TaggedObject.EvaluateStructuralWalls();
                return _coreDimensions;
            }
            set
            {
                _coreDimensions = value;
            }
        }


        public Balconies balconies { get { return Standards.TaggedObject.balconies; } }
        public ProceduralFloor currentFloor { get; set; }
        public List<ProceduralFloor> floors { get; set; }
        //public List<FloorLayoutState> floorLayoutStates { get; set; }
        public Polygon masterPolyline { get; set; }
        public bool Placed { get; set; }

        /// <summary>
        /// The maximum apartment width based on the brief of the current building
        /// </summary>
        public float currentMaximumWidth
        {
            get
            {
                var percentages = buildingElement.buildingData.percentages.ToList();//floors.Select(x => x.percentages).ToList();
                List<string> distinctTypes = new List<string>();
                int counter = 0;
                foreach (var item in percentages)
                {
                    foreach (var perc in item)
                    {
                        if (!distinctTypes.Contains(perc.Key) && perc.Key != "Other" && perc.Key != "Commercial")
                        {
                            distinctTypes.Add(perc.Key);
                            counter++;
                        }
                    }
                }

                if (counter != 0)
                {

                    distinctTypes = distinctTypes.OrderBy(x => Standards.TaggedObject.DesiredAreas[x]).ToList();
                    distinctTypes.Reverse();

                    return ((float)Standards.TaggedObject.DesiredAreas[distinctTypes[0]] / apartmentDepth) + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
                }
                else
                    return ((float)Standards.TaggedObject.DesiredAreas["1b2p"] / apartmentDepth) + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
            }
        }

        /// <summary>
        /// Whether at least one of its floors has geometry
        /// </summary>
        public bool hasGeometry
        {
            get
            {
                int count = 0;
                for (int i = 0; i < floors.Count; i++)
                {
                    if (floors[i].hasGeometry)
                    {
                        count++;
                    }
                }
                return count != 0;
            }
        }

        public float prevMinimumWidth { get; set; }

        /// <summary>
        /// The polyline parameters of the cores for the current building
        /// </summary>
        public List<float> currentCoresParameters
        {
            get
            {
                return floors[0].coresParams;
            }
        }

        /// <summary>
        /// The length between the corridors for the current floor
        /// </summary>
        public float currentCoreLength
        {
            get
            {
                return floors[0].coreLength;
            }
        }

        public float corridorWidth { get; set; }

        private float m_aptDepth;
        public float apartmentDepth
        {
            get
            {
                return m_aptDepth;
            }
            set
            {
                m_aptDepth = value;
                if (floors != null)
                {
                    for (int i = 0; i < floors.Count; i++)
                    {
                        floors[i].apartmentDepth = value;
                    }
                }
            }
        }

        public bool populateApartments { get; set; }
        public string buildingName { get; set; }
        public int index
        {
            get
            {
                return buildingManager.proceduralBuildings.IndexOf(this);
            }
        }
        public Transform repRootParent { get; set; }
        public List<GameObject> apartmentsInteriors { get; private set; }
        public ManufacturingSystem manufacturingSystem { get; private set; }
        public Text distanceUI { get; private set; }
        public Transform distanceUIParent { get; set; }
        public Material distancesMaterial { get; private set; }
        public List<ProceduralTower> towerConfigurations { get; set; }
        public int numberOfFloors
        {
            get
            {
                if (floors != null)
                {
                    if (floors[floors.Count - 1] != null && floors[floors.Count - 1].levels != null && floors[floors.Count - 1].levels.Length > 0)
                    {
                        return floors.Last().levels.Last() + 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else return 0;
            }
        }
        #endregion

        #region Private Fields and Properties
        private Polygon sitePolygon { get; set; }
        private ProceduralFloor lastAddedFloor { get; set; }
        private float plantRoomArea { get; set; }
        private WaitForEndOfFrame waitFrame { get; set; }
        public GameObject heightLine { get; private set; }
        private GameObject currentDistance { get; set; }
        private Transform distancesParent { get; set; }
        private float GEA = 0;
        private float GIA = 0;
        private float NIA = 0;
        private float GrossToNet = 0;
        private float ApartmentsArea = 0;
        private float FacadeArea = 0;
        private float FacadePerimeter = 0;
        private float WallToFloor = 0;
        private float NumberOfPeople = 0;
        private float Levels = 0;
        #endregion

        #region MonoBehaviour Methods
        public void Start()
        {
            //floors = new List<ProceduralFloor>();
            //floorLayoutStates = new List<FloorLayoutState>();

        }

        private void OnDestroy()
        {
            if (distanceUI != null)
            {
                Destroy(distanceUI.gameObject);
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes the building instance
        /// </summary>
        public void Initialize()
        {
            floors = new List<ProceduralFloor>();
            waitFrame = new WaitForEndOfFrame();
            distancesMaterial = Resources.Load("Materials/DistanceMaterial") as Material;
            distancesMaterial.color = Color.red;
        }

        /// <summary>
        /// Refreshes the building instance
        /// </summary>
        /// <param name="offsetDistance">The apartment depth</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator Refresh(float offsetDistance)
        {
            float maxWidth = currentMaximumWidth;
            prevMinimumWidth = maxWidth;
            apartmentDepth = offsetDistance;
            if (floors != null && floors.Count > 0)
            {
                for (int i = 0; i < floors.Count; i++)
                {
                    floors[i].corridorWidth = corridorWidth;
                    var tower = floors[i] as ProceduralTower;
                    if (tower == null)
                    {
                        var item = floors[i];
                        var perc = floors[i].percentages;
                        item.percentages = perc;
                        if (i == 0)
                        {
                            if (item.centreLine == null)
                            {
                                var polygon = UseMasterPolyline(i);
                                SetCentreLineToFloor(item, polygon, i);
                            }
                            item.apartmentDepth = offsetDistance;
                            yield return StartCoroutine(item.GenerateGeometries(maxWidth));
                            buildingManager.apartmentTypesChart.designData = item;
                        }
                        else
                        {
                            var yVal = buildingElement.GetYForLevel(item.levels[0], i);
                            item.yVal = yVal;
                            item.transform.position = new Vector3(item.transform.position.x, yVal, item.transform.position.z);
                            if (item.centreLine == null)
                            {
                                var polygon = UseMasterPolyline(item.id);
                                SetCentreLineToFloor(item, polygon, i);
                            }
                            item.apartmentDepth = offsetDistance;
                            yield return StartCoroutine(item.GenerateGeometries(floors[0], maxWidth));
                            buildingManager.apartmentTypesChart.designData = item;
                        }
                    }
                    else
                    {
                        if (i > 0)
                        {
                            tower.apartmentDepth = offsetDistance;
                            tower.GenerateGeometries();
                            yield return waitFrame;
                            buildingManager.apartmentTypesChart.designData = tower;
                        }
                    }
                }
                floors[0].GeneratePlantRoom();

                if (heightLine == null)
                {
                    if (buildingType == BuildingType.Linear)
                        AddHeightLine(masterPolyline);
                    else
                        AddHeightLine();
                }
                UpdateHeightLine(masterPolyline);
                buildingManager.CheckSystemConstraints(this);
            }
        }

        /// <summary>
        /// Populates the apartments of the floors with their interior
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator PopulateInterior()
        {
            populateApartments = !populateApartments;
            yield return StartCoroutine(UpdateInternalLayout());
        }

        /// <summary>
        /// Populates the apartments of the floors with their interior
        /// </summary>
        /// <param name="show">Turn interior on or off</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator PopulateInterior(bool show)
        {
            populateApartments = show;
            yield return StartCoroutine(UpdateInternalLayout());
        }

        /// <summary>
        /// Updates the interior of the apartments of the floors
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator UpdateInternalLayout()
        {
            yield return UpdateInternalLayout(populateApartments);
        }

        /// <summary>
        /// Used to resolve a Special Apartment to its neighbours
        /// </summary>
        /// <param name="aptsToCombine">The apartments to be combined</param>
        public void OnResolveApartmentWithNext(List<BaseDesignData> aptsToCombine)
        {
            buildingManager.SetSwapApartments(aptsToCombine);
            buildingManager.CombineApartments();
        }

        /// <summary>
        /// Returns a detailed list of all the modules in the building
        /// </summary>
        /// <returns>List of strings</returns>
        public List<string> GetModuleDetails()
        {
            List<string> lines = new List<string>();
            //ModuleID,ModuleWidth,ModuleDepth,ModuleHeight,ApartmentID,ApartmentType

            for (int i = 0; i < apartmentsInteriors.Count; i++)
            {
                for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                {
                    var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                    lines.Add(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", obj.name, obj.transform.localScale.x, obj.transform.localScale.z, obj.transform.localScale.y, "N/A", apartmentsInteriors[i].GetComponent<ProceduralApartmentLayout>().apartmentType, obj.transform.position.x, obj.transform.position.y, obj.transform.position.z, obj.transform.eulerAngles.x, obj.transform.eulerAngles.y, obj.transform.eulerAngles.z));
                }
            }

            return lines;
        }

        /// <summary>
        /// Returns the number for each different module type and size
        /// </summary>
        /// <returns>String Int Dictionary</returns>
        public Dictionary<string, int> GetModuleSchedule()
        {
            Dictionary<string, int> modSched = new Dictionary<string, int>();

            for (int i = 0; i < apartmentsInteriors.Count; i++)
            {
                for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                {
                    var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                    string name = obj.name + ":" + obj.transform.localScale.x + "," + obj.transform.localScale.z + "," + obj.transform.localScale.y;
                    if (modSched.ContainsKey(name))
                    {
                        modSched[name]++;
                    }
                    else
                    {
                        modSched.Add(name, 1);
                    }
                }
            }

            return modSched;
        }

        /// <summary>
        /// Returns the different types and numbers of panels required for the building
        /// </summary>
        /// <returns>String Int Dictionary</returns>
        public Dictionary<string, int> GetPanelSchedule()
        {
            Dictionary<string, int> panSched = new Dictionary<string, int>();

            for (int i = 0; i < apartmentsInteriors.Count; i++)
            {
                for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(2).childCount; j++)
                {
                    var obj = apartmentsInteriors[i].transform.GetChild(2).GetChild(j);
                    string name = "Panel:" + (obj.transform.localScale.x * obj.transform.localScale.z * obj.transform.localScale.y) / 0.15f + ":" + obj.transform.localScale.x + "," + obj.transform.localScale.z + "," + obj.transform.localScale.y;
                    if (panSched.ContainsKey(name))
                    {
                        panSched[name]++;
                    }
                    else
                    {
                        panSched.Add(name, 1);
                    }
                }
            }

            return panSched;
        }

        /// <summary>
        /// Logs the Undo-able resolve action
        /// </summary>
        /// <param name="action">The action to be logged</param>
        public void SetResolveAction(ResolveAction action)
        {
            buildingManager.SetResolveAction(action);
        }

        /// <summary>
        /// Called when this building has been deleted
        /// </summary>
        public void Delete()
        {
            Destroy(repRootParent.gameObject);
        }

        /// <summary>
        /// Called when this building has been selected
        /// </summary>
        public void Select()
        {
            StartCoroutine(OnSelect());
        }

        /// <summary>
        /// Called when this building is being deselected
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator Deselect()
        {

            if (previewMode == PreviewMode.Floors)
            {
                if (populateApartments)
                {
                    populateApartments = false;
                    yield return StartCoroutine(UpdateInternalLayout(false));
                }
                for (int i = 0; i < floors.Count; i++)
                {
                    if (floors[i].centreLine != null)
                    {
                        floors[i].centreLine.gameObject.SetActive(false);
                    }
                    floors[i].gameObject.SetActive(true);
                    floors[i].ToggleChildren(false);
                }
            }
        }

        /// <summary>
        /// Called when a floor from this building has been deleted
        /// </summary>
        /// <param name="floor">The deleted floor</param>
        public void OnFloorDeleted(ProceduralFloor floor)
        {
            floors.Remove(floor);
        }

        /// <summary>
        /// Called when a floor from this building has been selected
        /// </summary>
        /// <param name="floor">The selected floor</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator OnFloorSelected(ProceduralFloor floor)
        {
            if (currentFloor != floor)
            {
                if (previewMode == PreviewMode.Floors)
                {
                    if (populateApartments)
                    {
                        yield return StartCoroutine(UpdateInternalLayout(false));
                    }


                    if (currentFloor.centreLine != null)
                    {
                        currentFloor.centreLine.gameObject.SetActive(false);
                    }
                    currentFloor.gameObject.SetActive(true);
                    currentFloor.ToggleChildren(false);

                    if (floor.centreLine != null)
                    {
                        floor.centreLine.gameObject.SetActive(true);
                    }
                    floor.ToggleChildren(true);

                    if (populateApartments)
                    {
                        yield return StartCoroutine(UpdateInternalLayout(true));
                    }
                }
                currentFloor = floor;

                buildingManager.SetSelectedFloor(this, floors.IndexOf(currentFloor));

                //StartCoroutine(buildingManager.SetSelectedFloor(this, floors.IndexOf(currentFloor)));

            }
        }

        /// <summary>
        /// Returns the total number of levels for this building
        /// </summary>
        /// <returns>INT</returns>
        public int GetTotalNumberOfLevels()
        {
            return buildingElement.buildingData.heightPerLevel.Count;
        }

        /// <summary>
        /// Returns the minimum floor to floor height for this building
        /// </summary>
        /// <returns>FLOAT</returns>
        public float GetMinimumFloorToFloor()
        {
            float f2fMin = float.MaxValue;
            for (int i = 1; i < buildingElement.buildingData.floorHeights.Count; i++)
            {
                if (buildingElement.buildingData.floorHeights[i] < f2fMin)
                {
                    f2fMin = buildingElement.buildingData.floorHeights[i];
                }
            }
            return f2fMin;
        }

        /// <summary>
        /// Request an overall update from the procedural building manager
        /// </summary>
        public void RequestOverallUpdate()
        {
            StartCoroutine(buildingManager.OverallUpdate());
        }

        /// <summary>
        /// Return Raw Data fro each floor as a list of string lines
        /// </summary>
        /// <returns>List of strings</returns>
        public List<string> GetPerFloorRawData()
        {
            List<string> lines = new List<string>();

            GEA = 0;
            GIA = 0;
            NIA = 0;
            GrossToNet = 0;
            ApartmentsArea = 0;
            FacadeArea = 0;
            FacadePerimeter = 0;
            WallToFloor = 0;
            NumberOfPeople = 0;
            Levels = 0;

            int[] aptTypes = new int[Standards.TaggedObject.ApartmentTypesMinimumSizes.Keys.Count];

            for (int i = 0; i < floors.Count; i++)
            {
                GEA += floors[i].GEA * floors[i].levels.Length;
                GIA += floors[i].GIA * floors[i].levels.Length;
                NIA += floors[i].NIA * floors[i].levels.Length;
                ApartmentsArea += floors[i].apartmentsArea * floors[i].levels.Length;
                FacadeArea += floors[i].facadeArea * floors[i].levels.Length;
                FacadePerimeter += floors[i].facadePerimeter * floors[i].levels.Length;
                NumberOfPeople += floors[i].numberOfPeople * floors[i].levels.Length;
                var aptNums = floors[i].apartmentNumbers;
                for (int j = 0; j < aptNums.Count; j++)
                {
                    aptTypes[j] += aptNums[j] * floors[i].levels.Length;
                }
            }

            GrossToNet = NIA / GIA;
            WallToFloor = (FacadeArea / GEA) * 100;
            Levels = floors[floors.Count - 1].levels[floors[floors.Count - 1].levels.Length - 1] + 1;

            var aptTyp = aptTypes.Select(x => x.ToString()).ToArray();

            lines.Add(index + "," + buildingName + ",," + string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}", Math.Round(GEA), Math.Round(GIA), Math.Round(NIA), Math.Round(GrossToNet * 100, 2), Math.Round(ApartmentsArea), Math.Round(FacadeArea), Math.Round(FacadePerimeter), Math.Round(WallToFloor, 2), NumberOfPeople, "", Levels, "", "", string.Join(",", aptTyp)));


            for (int i = 0; i < floors.Count; i++)
            {
                for (int j = 0; j < floors[i].levels.Length; j++)
                {
                    lines.Add(index + "," + buildingName + "," + floors[i].key + "," + floors[i].GetFloorStats(j));
                }
            }
            return lines;
        }

        /// <summary>
        /// Populates the interior of the apartments of all floors for exporting them
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator PopulateForExport()
        {
            for (int i = 0; i < floors.Count; i++)
            {
                floors[i].PopulateApartments(true, repRootParent, true);
            }
            yield return waitFrame;
            apartmentsInteriors = new List<GameObject>();
            for (int i = 0; i < floors.Count; i++)
            {
                int floorNum = floors[i].levels.Length;
                List<GameObject> objs = new List<GameObject>();
                for (int j = 0; j < floorNum; j++)
                {
                    if (j > 0)
                    {
                        objs.Add(Instantiate(repRootParent.GetChild(i).gameObject));
                        objs[j].transform.position += new Vector3(0, floors[i].floor2floor * j, 0);
                    }
                    else
                    {
                        objs.Add(repRootParent.GetChild(i).gameObject);
                    }
                }

                for (int j = 0; j < objs.Count; j++)
                {
                    if (j > 0)
                    {
                        objs[j].transform.SetParent(repRootParent);
                    }
                    for (int k = 0; k < objs[j].transform.childCount; k++)
                    {
                        apartmentsInteriors.Add(objs[j].transform.GetChild(k).gameObject);
                    }
                }
            }
        }

        /// <summary>
        /// Returns the maximum module width for this building
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumModuleWidth()
        {
            float maxWidth = float.MinValue;

            for (int i = 1; i < floors.Count; i++)
            {
                float width = floors[i].GetMaximumModuleWidth();
                if (width > maxWidth)
                {
                    maxWidth = width;
                }
            }

            return maxWidth;
        }

        /// <summary>
        /// Returns the maximum line-load span in the building
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumLineLoadSpan()
        {
            float maxSpan = float.MinValue;

            for (int i = 1; i < floors.Count; i++)
            {
                float width = floors[i].GetMaximumLineLoadSpan();
                if (width > maxSpan)
                {
                    maxSpan = width;
                }
            }

            return maxSpan;
        }

        /// <summary>
        /// Returns the mximum panel width for this building
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumPanelLength()
        {
            float maxWidth = float.MinValue;

            for (int i = 0; i < floors.Count; i++)
            {
                float width = floors[i].GetMaximumPanelWidth();
                if (width > maxWidth)
                {
                    maxWidth = width;
                }
            }

            return maxWidth;
        }

        /// <summary>
        /// Updates the interior shown for each apartment in this building
        /// </summary>
        /// <param name="show">Whether the interiors should be shown or not</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator UpdateInternalLayout(bool show)
        {
            if (repRootParent.childCount > 0)
            {
                for (int i = 0; i < repRootParent.childCount; i++)
                {
                    Destroy(repRootParent.GetChild(i).gameObject);
                }
            }

            if (show)
            {
                for (int i = 0; i < floors.Count; i++)
                {
                    floors[i].PopulateApartments(show, repRootParent, previewMode == PreviewMode.Floors);
                }

                yield return waitFrame;
                switch (previewMode)
                {
                    case PreviewMode.Floors:
                        apartmentsInteriors = new List<GameObject>();
                        for (int i = 0; i < repRootParent.childCount; i++)
                        {
                            for (int j = 0; j < repRootParent.GetChild(i).childCount; j++)
                            {
                                apartmentsInteriors.Add(repRootParent.GetChild(i).GetChild(j).gameObject);
                            }
                        }
                        break;
                    case PreviewMode.Buildings:
                        apartmentsInteriors = new List<GameObject>();
                        for (int i = 0; i < floors.Count; i++)
                        {
                            int floorNum = floors[i].levels.Length;
                            List<GameObject> objs = new List<GameObject>();
                            for (int j = 0; j < floorNum; j++)
                            {
                                if (j > 0)
                                {
                                    objs.Add(Instantiate(repRootParent.GetChild(i).gameObject));
                                    objs[j].transform.position += new Vector3(0, floors[i].floor2floor * j, 0);
                                }
                                else
                                {
                                    objs.Add(repRootParent.GetChild(i).gameObject);
                                }
                            }

                            for (int j = 0; j < objs.Count; j++)
                            {
                                if (j > 0)
                                {
                                    objs[j].transform.SetParent(repRootParent);
                                }
                                for (int k = 0; k < objs[j].transform.childCount; k++)
                                {
                                    apartmentsInteriors.Add(objs[j].transform.GetChild(k).gameObject);
                                }
                            }
                        }
                        break;
                }
            }
            else
            {
                for (int i = 0; i < repRootParent.childCount; i++)
                {
                    DestroyImmediate(repRootParent.GetChild(i).gameObject);
                }

                if (previewMode == PreviewMode.Floors)
                {
                    if (currentFloor.hasGeometry)
                    {
                        currentFloor.PopulateApartments(show);
                    }
                }
                else if (previewMode == PreviewMode.Buildings)
                {
                    for (int i = 0; i < floors.Count; i++)
                    {
                        if (floors[i].hasGeometry)
                        {
                            floors[i].PopulateApartments(show);
                        }
                    }
                    //foreach (var item in floors)
                    //{
                    //    if (item.hasGeometry)
                    //    {
                    //        item.PopulateApartments(show);
                    //    }
                    //}
                }
                yield return waitFrame;
            }
        }

        /// <summary>
        /// Sets the displayed manufacturing system
        /// </summary>
        /// <param name="manufacturingSystem">The selected manufacturing system</param>
        public void ToggleManufacturingSystem(ManufacturingSystem manufacturingSystem)
        {
            this.manufacturingSystem = manufacturingSystem;
            switch (previewMode)
            {
                case PreviewMode.Buildings:
                    if (populateApartments)
                    {
                        for (int i = 0; i < floors.Count; i++)
                        {
                            floors[i].SetManufacturingSystem(manufacturingSystem);
                        }
                        //foreach (var item in floors)
                        //{
                        //    item.SetManufacturingSystem(manufacturingSystem);
                        //}
                        switch (manufacturingSystem)
                        {
                            case ManufacturingSystem.VolumetricModules:
                                for (int i = 0; i < apartmentsInteriors.Count; i++)
                                {
                                    var procedural = apartmentsInteriors[i].transform;
                                    for (int k = 1; k <= 4; k++)
                                    {
                                        if (k == 1)
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(true);
                                        }
                                        else
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(false);
                                        }
                                    }
                                }
                                for (int i = 0; i < floors.Count; i++)
                                {
                                    floors[i].SetTransparentApartmentMaterial(false);
                                }
                                break;
                            case ManufacturingSystem.Panels:
                                for (int i = 0; i < apartmentsInteriors.Count; i++)
                                {
                                    var procedural = apartmentsInteriors[i].transform;
                                    for (int k = 1; k <= 4; k++)
                                    {
                                        if (k == 2)
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(true);
                                        }
                                        else
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(false);
                                        }
                                    }
                                }
                                //for (int i = 0; i < floors.Count; i++)
                                //{
                                //    floors[i].SetTransparentApartmentMaterial(true);
                                //}
                                break;
                            case ManufacturingSystem.LineLoads:
                                for (int i = 0; i < apartmentsInteriors.Count; i++)
                                {
                                    var procedural = apartmentsInteriors[i].transform;
                                    for (int k = 1; k <= 4; k++)
                                    {
                                        if (k == 3)
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(true);
                                        }
                                        else
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(false);
                                        }
                                    }
                                }
                                for (int i = 0; i < floors.Count; i++)
                                {
                                    floors[i].SetTransparentApartmentMaterial(true);
                                }
                                break;
                            case ManufacturingSystem.Off:
                                for (int i = 0; i < apartmentsInteriors.Count; i++)
                                {
                                    var procedural = apartmentsInteriors[i].transform;
                                    for (int k = 1; k <= 4; k++)
                                    {
                                        if (k == 4)
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(true);
                                        }
                                        else
                                        {
                                            procedural.GetChild(k).gameObject.SetActive(false);
                                        }
                                    }
                                }
                                for (int i = 0; i < floors.Count; i++)
                                {
                                    floors[i].SetTransparentApartmentMaterial(false);
                                }
                                break;
                        }
                    }
                    break;
                case PreviewMode.Floors:
                    if (populateApartments)
                    {
                        for (int i = 0; i < floors.Count; i++)
                        {
                            floors[i].SetManufacturingSystem(manufacturingSystem);
                        }
                        //foreach (var item in floors)
                        //{
                        //    item.SetManufacturingSystem(manufacturingSystem);
                        //}
                    }
                    break;
            }

        }

        /// <summary>
        /// Returns the numbers for all different modules
        /// </summary>
        /// <returns>String - Float Dictionary</returns>
        public Dictionary<string, float> GetVerticalModulesNumber()
        {
            if (populateApartments)
            {
                Dictionary<string, float> modulesSizes = new Dictionary<string, float>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                        if (modulesSizes.ContainsKey(obj.gameObject.name))
                        {
                            modulesSizes[obj.gameObject.name] += (obj.transform.localScale.x * obj.transform.localScale.z);
                        }
                        else
                        {
                            modulesSizes.Add(obj.gameObject.name, (obj.transform.localScale.x * obj.transform.localScale.z));
                        }
                    }
                }
                return modulesSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the numbers for all different panels
        /// </summary>
        /// <returns>String - Int Dictionary</returns>
        public Dictionary<string, int> GetPanelsNumbers()
        {
            if (populateApartments)
            {
                Dictionary<string, int> panelsSizes = new Dictionary<string, int>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(2).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(2).GetChild(j);
                        if (panelsSizes.ContainsKey(obj.gameObject.name))
                        {
                            panelsSizes[obj.gameObject.name]++;
                        }
                        else
                        {
                            panelsSizes.Add(obj.gameObject.name, 1);
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the sizes for all different Modules
        /// </summary>
        /// <returns>String - Int Dictionary</returns>
        public Dictionary<string, int> GetVerticalModulesSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, int> modulesSizes = new Dictionary<string, int>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                        var m_name = string.Format("{0}/{1}/{2}", obj.gameObject.name, obj.localScale.x, obj.localScale.z);
                        if (modulesSizes.ContainsKey(m_name))
                        {
                            modulesSizes[m_name]++;//Add( new float[] { obj.localScale.x, obj.localScale.z });
                        }
                        else
                        {
                            modulesSizes.Add(m_name, 1);//new List<float[]>() { new float[] { obj.localScale.x, obj.localScale.z } });
                        }
                    }
                }
                return modulesSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the sizes for all different facade panels
        /// </summary>
        /// <returns>String - Int Dictionary</returns>
        public Dictionary<string, int> GetFacadePanelsSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, int> panelsSizes = new Dictionary<string, int>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(2).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(2).GetChild(j);
                        var m_name = string.Format("{0}/{1}/{2}", obj.gameObject.name, obj.localScale.x, obj.localScale.y);
                        if (panelsSizes.ContainsKey(m_name))
                        {
                            panelsSizes[m_name]++;//Add( new float[] { obj.localScale.x, obj.localScale.z });
                        }
                        else
                        {
                            panelsSizes.Add(m_name, 1);//new List<float[]>() { new float[] { obj.localScale.x, obj.localScale.z } });
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the sizes for the volumetric modules
        /// </summary>
        /// <returns>String - List of float Dictionary</returns>
        public Dictionary<string, List<float>> GetVolumetricSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, List<float>> panelsSizes = new Dictionary<string, List<float>>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(1).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(1).GetChild(j);
                        if (panelsSizes.ContainsKey(obj.gameObject.name))
                        {
                            panelsSizes[obj.gameObject.name].Add((float)Math.Round(obj.localScale.x, 2));
                        }
                        else
                        {
                            panelsSizes.Add(obj.gameObject.name, new List<float> { (float)Math.Round(obj.localScale.x, 2) });
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the maximum apartment depth for this building
        /// </summary>
        /// <returns>FLOAT</returns>
        public float GetMaxDepth()
        {
            float maxDepth = float.MinValue;
            for (int i = 0; i < floors.Count; i++)
            {
                if (floors[i].hasGeometry)
                {
                    var depth = floors[i].MaxAptDepth;
                    if (maxDepth < depth)
                    {
                        maxDepth = depth;
                    }
                }
            }
            return maxDepth;
        }

        /// <summary>
        /// Returns the sizes for the facade panels
        /// </summary>
        /// <returns>String - List of float Dictionary</returns>
        public Dictionary<string, List<float>> GetPanelsSizes()
        {
            if (populateApartments)
            {
                Dictionary<string, List<float>> panelsSizes = new Dictionary<string, List<float>>();
                for (int i = 0; i < apartmentsInteriors.Count; i++)
                {
                    for (int j = 0; j < apartmentsInteriors[i].transform.GetChild(2).childCount; j++)
                    {
                        var obj = apartmentsInteriors[i].transform.GetChild(2).GetChild(j);
                        if (panelsSizes.ContainsKey(obj.gameObject.name))
                        {
                            panelsSizes[obj.gameObject.name].Add((float)Math.Round(obj.localScale.x, 2));
                        }
                        else
                        {
                            panelsSizes.Add(obj.gameObject.name, new List<float> { (float)Math.Round(obj.localScale.x, 2) });
                        }
                    }
                }
                return panelsSizes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the number of apartments for this building
        /// </summary>
        /// <returns>String - Float Dictionary</returns>
        public Dictionary<string, float> GetApartmentNumbers(bool perLevel = false)
        {
            Dictionary<string, float> areas = new Dictionary<string, float>();

            for (int i = 0; i < floors.Count; i++)
            {
                var floor = floors[i];
                if (floor != null && floor.apartments != null)
                {
                    var stats = floor.ApartmentStats;
                    foreach (var item in stats)
                    {
                        if (areas.ContainsKey(item.Key))
                        {
                            if (perLevel)
                            {
                                areas[item.Key] += item.Value[0];
                            }
                            else
                            {
                                areas[item.Key] += item.Value[0] * floor.levels.Length;
                            }
                        }
                        else
                        {
                            if (perLevel)
                            {
                                areas.Add(item.Key, item.Value[0]);
                            }
                            else
                            {
                                areas.Add(item.Key, item.Value[0] * floor.levels.Length);
                            }
                        }
                    }
                }
            }
            return areas;
        }

        /// <summary>
        /// Returns the areas for the apartments in this building
        /// </summary>
        /// <returns>String - Float Dictionary</returns>
        public Dictionary<string, float> GetApartmentAreas()
        {
            Dictionary<string, float> areas = new Dictionary<string, float>();

            for (int i = 0; i < floors.Count; i++)
            {
                var floor = floors[i];
                if (floor != null && floor.apartments != null)
                {
                    var stats = floor.ApartmentStats;
                    foreach (var item in stats)
                    {
                        if (areas.ContainsKey(item.Key))
                        {
                            areas[item.Key] += item.Value[1] * floor.levels.Length;
                        }
                        else
                        {
                            areas.Add(item.Key, item.Value[1] * floor.levels.Length);
                        }
                    }
                }
            }
            return areas;
        }

        /// <summary>
        /// Returns the number of apartments for the selected floor
        /// </summary>
        /// <returns>String - Float Dictionary</returns>
        public Dictionary<string, float> GetCurrentApartmentNumbers()
        {
            Dictionary<string, float> areas = new Dictionary<string, float>();

            var floor = currentFloor;
            if (floor != null && floor.apartments != null)
            {
                var stats = floor.ApartmentStats;
                foreach (var item in stats)
                {
                    if (areas.ContainsKey(item.Key))
                    {
                        areas[item.Key] += item.Value[0];// * floor.levels.Length;
                    }
                    else
                    {
                        areas.Add(item.Key, item.Value[0]);// * floor.levels.Length);
                    }
                }
            }

            return areas;
        }

        /// <summary>
        /// Returns the number of apartments for this building as string
        /// </summary>
        /// <returns>String</returns>
        public string GetApartmentNumbersText()
        {
            string info = String.Empty;

            var areas = GetApartmentNumbers();
            float totalAptNumber = 0.0f;
            foreach (var item in areas)
            {
                totalAptNumber += item.Value;
            }
            info = "Total = " + totalAptNumber + "\n";
            foreach (var item in areas)
            {
                info += string.Format("{0} = {1}, {2}%\n", item.Key, item.Value, Math.Round((item.Value / totalAptNumber) * 100, 2));
            }

            return info;
        }

        /// <summary>
        /// Returns the apartments of a specific type for this building
        /// </summary>
        /// <param name="type">The apartment type</param>
        /// <returns>List of Apartments</returns>
        public List<ApartmentUnity> GetApartments(string type)
        {
            List<ApartmentUnity> apartments = new List<ApartmentUnity>();

            for (int i = 0; i < floors.Count; i++)
            {
                var floor = floors[i];
                for (int j = 0; j < floor.apartments.Count; j++)
                {
                    if (floor.apartments[j].ApartmentType == type)
                    {
                        apartments.Add(floor.apartments[j]);
                    }
                }
            }

            return apartments;
        }

        /// <summary>
        /// Returns the statistics for this building
        /// </summary>
        /// <param name="GEA">Gross External Area</param>
        /// <param name="GIA">Gross Internal Area</param>
        /// <param name="NIA">NET Internal Area</param>
        /// <param name="totalHeight">The total height</param>
        /// <param name="unitDensity">The density as units per hectare</param>
        /// <param name="habDensity">The density as habitable rooms per hectare</param>
        /// <param name="plantRoomArea">The plant-room area</param>
        /// <param name="aptAreas">The areas of the apartments</param>
        /// <param name="aptNumbers">The numbers of the apartments</param>
        /// <param name="commercialArea">The area of the commercial use</param>
        /// <param name="facadeArea">The area of the facade</param>
        /// <param name="groundFloorArea">The total area of the ground floor</param>
        /// <param name="otherArea">The area of the ground floor which is not specifically assigned a use</param>
        /// <param name="receptionArea">The area of the ground floor which is assigned for reception</param>
        public void GetCurrentBuildingStats(out float GEA, out float GIA, out float NIA, out float totalHeight, out float unitDensity, out float habDensity, out float plantRoomArea, out Dictionary<string, float> aptAreas, out Dictionary<string, float> aptNumbers, out float groundFloorArea, out float receptionArea, out float commercialArea, out float otherArea, out float facadeArea)
        {
            GEA = 0.0f;
            GIA = 0.0f;
            NIA = 0.0f;
            totalHeight = 0.0f;
            unitDensity = 0.0f;
            habDensity = 0.0f;
            groundFloorArea = 0f;
            aptAreas = new Dictionary<string, float>();
            aptNumbers = new Dictionary<string, float>();
            plantRoomArea = 0;
            receptionArea = 0;
            commercialArea = 0;
            otherArea = 0;
            facadeArea = 0;

            if (floors != null && floors.Count > 0)
            {

                groundFloorArea = floors[0].GEA;
                var site = buildingManager.sitePolygon;
                if (site != null)
                {
                    for (int i = 0; i < floors.Count; i++)
                    {
                        if (floors[i].apartments != null)
                        {
                            unitDensity += (floors[i].apartments.Count * floors[i].levels.Length);
                            habDensity += (floors[i].habitRoomsCount * floors[i].levels.Length);
                        }
                    }
                }

                totalHeight = 0;
                for (int i = 0; i < floors.Count; i++)
                {
                    try
                    {
                        var floor = floors[i];
                        GIA += floor.GIA * floor.levels.Length;
                        GEA += floor.GEA * floor.levels.Length;
                        NIA += floor.NIA * floor.levels.Length;
                        facadeArea += floors[i].facadeArea * floors[i].levels.Length;
                        if (i == 0)
                        {
                            if (floors[i].percentages.ContainsKey("Commercial"))
                                commercialArea = floors[i].percentages["Commercial"] / 100f * (floors[0].GIA - receptionArea);
                            else if (floors[i].percentages.ContainsKey("Other"))
                                otherArea = floors[i].percentages["Other"] / 100f * (floors[0].GIA - receptionArea);
                        }
                        else
                        {
                            if (floors[i].percentages.ContainsKey("Commercial"))
                                commercialArea = floors[i].percentages["Commercial"] / 100f * (floors[i].GIA);
                            else if (floors[i].percentages.ContainsKey("Other"))
                                otherArea = floors[i].percentages["Other"] / 100f * (floors[i].GIA);
                        }
                        totalHeight += floor.levels.Length * floor.floor2floor;
                        if (i > 0)
                        {
                            var stats = floor.ApartmentStats;

                            foreach (var item in stats)
                            {
                                if (aptAreas.ContainsKey(item.Key))
                                {
                                    aptAreas[item.Key] += item.Value[1] * floor.levels.Length;
                                }
                                else
                                {
                                    aptAreas.Add(item.Key, item.Value[1] * floor.levels.Length);
                                }

                                if (aptNumbers.ContainsKey(item.Key))
                                {
                                    aptNumbers[item.Key] += item.Value[0] * floor.levels.Length;
                                }
                                else
                                {
                                    aptNumbers.Add(item.Key, item.Value[0] * floor.levels.Length);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                }

                totalHeight += (floors.Count - 1) * 0.5f;
                plantRoomArea = GetBuildingPlantArea();
                receptionArea = GetBuildingReceptionArea();
            }
        }

        /// <summary>
        /// Returns the statistics for the apartments in this building
        /// </summary>
        /// <returns>String - Floar Array Dictionary</returns>
        public Dictionary<string, float[]> GetApartmentStats()
        {
            Dictionary<string, float[]> stats = new Dictionary<string, float[]>();

            for (int i = 1; i < floors.Count; i++)
            {
                var m_stats = floors[i].ApartmentStats;
                foreach (var item in m_stats)
                {
                    if (stats.ContainsKey(item.Key))
                    {
                        stats[item.Key][0] += item.Value[0] * floors[i].levels.Length;
                        stats[item.Key][1] += item.Value[1] * floors[i].levels.Length;
                    }
                    else
                    {
                        stats.Add(item.Key, new float[] { item.Value[0] * floors[i].levels.Length, item.Value[1] * floors[i].levels.Length });
                    }
                }
            }

            return stats;
        }

        /// <summary>
        /// Returns the statistics for this building
        /// </summary>
        /// <param name="NIA">Net Internal Area</param>
        /// <param name="GIA">Gross Internal Area</param>
        /// <returns>String Array</returns>
        public string[] GetCurrentBuildingStats(out float NIA, out float GIA)
        {
            string[] info = new string[2];
            GIA = 0.0f;
            NIA = 0.0f;
            if (floors != null && floors.Count > 0)
            {
                info[0] = string.Format("Summary - {0}", buildingName);
                var site = buildingManager.sitePolygon;
                float unitsPerHectare = -1;
                float habRoomsPerHectare = -1;
                if (site != null)
                {
                    for (int i = 0; i < floors.Count; i++)
                    {
                        if (floors[i].apartments != null)
                        {
                            unitsPerHectare += (floors[i].apartments.Count * floors[i].levels.Length);
                            habRoomsPerHectare += (floors[i].habitRoomsCount * floors[i].levels.Length);
                        }
                    }
                    unitsPerHectare /= (site.Area / 10000.0f);
                    habRoomsPerHectare /= (site.Area / 10000.0f);
                }
                GEA = 0.0f;

                int numberOfLevels = 0;
                float totalHeight = 0;
                float facadeArea = 0;
                for (int i = 0; i < floors.Count; i++)
                {
                    try
                    {
                        var floor = floors[i];
                        GIA += floor.GIA * floor.levels.Length;
                        GEA += floor.GEA * floor.levels.Length;
                        NIA += floor.NIA * floor.levels.Length;
                        numberOfLevels += floor.levels.Length;
                        totalHeight += floor.levels.Length * floor.floor2floor;
                        facadeArea += floor.facadeArea * floor.levels.Length;
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                }

                totalHeight += (floors.Count - 1) * 0.5f;
                float plantRoomArea = GetBuildingPlantArea();
                float receptionArea = GetBuildingReceptionArea();
                float otherArea = GetOtherArea(receptionArea);
                float commercialArea = GetCommercialArea(receptionArea);

                info[1] = string.Format(
                    "GEA = {0}m\xB2 / {13}ft\xB2\n" +
                    "GIA = {1}m\xB2 / {14}ft\xB2\n" +
                    "NIA = {2}m\xB2 / {15}ft\xB2\n" +
                    "Net to gross = {8}%\n" +
                    "GIA to GEA = {9}%\n" +
                    "Plant-Room Area = {4}m\xB2 / {16}ft\xB2\n" +
                    "Entrance = {11}m\xB2 / {17}ft\xB2\n" +
                    "Other = {12}m\xB2 / {18}ft\xB2\n" +
                    "Commercial = {19}m\xB2 / {20}ft\xB2\n" +
                    "Number of levels = {3}\n" +
                    "Building height = {5}m\n"+
                    "Wall to floor = {21}",
                    String.Format(numbersFormat, Mathf.Round(GEA)),
                    String.Format(numbersFormat, Mathf.Round(GIA)),
                    String.Format(numbersFormat, Mathf.Round(NIA)),
                    numberOfLevels,
                    String.Format(numbersFormat, Math.Round(plantRoomArea)),
                    totalHeight,
                    (unitsPerHectare != -1) ? Math.Round(unitsPerHectare).ToString() : "N/A",
                    (habRoomsPerHectare != -1) ? Math.Round(habRoomsPerHectare).ToString() : "N/A",
                    Math.Round((Mathf.Round(NIA) / Mathf.Round(GIA)) * 100, 2),
                    Math.Round((Mathf.Round(GIA) / Mathf.Round(GEA)) * 100, 2),
                    (site != null) ? Math.Round((floors[0].GEA / site.Area) * 100, 2).ToString() : "N/A",
                    String.Format(numbersFormat, Math.Round(receptionArea)),
                    String.Format(numbersFormat, Math.Round(otherArea)),
                    String.Format(numbersFormat, Mathf.Round(GEA * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Mathf.Round(GIA * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Mathf.Round(NIA * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round(plantRoomArea * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round(receptionArea * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round((otherArea) * ProceduralBuildingManager.SqMeterToSqFoot)),
                    String.Format(numbersFormat, Math.Round((commercialArea))),
                    String.Format(numbersFormat, Math.Round((commercialArea) * ProceduralBuildingManager.SqMeterToSqFoot)),
                    Math.Round(facadeArea/GEA,2)
                    );

                info[1] += "\n";
                info[1] += GetApartmentNumbersText();
                info[1] += "\n";
                info[1] += GetBriefDifference();
            }
            return info;
        }

        /// <summary>
        /// Gets the values for the system analysis
        /// </summary>
        /// <param name="minF2f">The minimum floor to floor height in the building</param>
        /// <param name="maxF2f">The maximum floor to floor height in the building</param>
        /// <param name="maxModule">The maximum Module Width / Panel Length in the building</param>
        /// <param name="maxSpan">The maximum line-load span in the building</param>
        /// <param name="storiesNum">The number of stories in the building</param>
        public void GetSystemNumbers(ref float minF2f, ref float maxF2f, ref float maxModule, ref float maxSpan, ref int storiesNum)
        {
            storiesNum = buildingElement.buildingData.heightPerLevel.Count;

            minF2f = float.MaxValue;
            maxF2f = float.MinValue;
            for (int i = 1; i < buildingElement.buildingData.floorHeights.Count; i++)
            {
                if (buildingElement.buildingData.floorHeights[i] < minF2f)
                {
                    minF2f = buildingElement.buildingData.floorHeights[i];
                }
                if (buildingElement.buildingData.floorHeights[i] > maxF2f)
                {
                    maxF2f = buildingElement.buildingData.floorHeights[i];
                }
            }

            maxModule = float.MinValue;
            maxSpan = float.MinValue;
            for (int i = 1; i < floors.Count; i++)
            {
                float width = 0;
                float span = 0;
                floors[i].GetSystemNumbers(ref width, ref span);
                if (width > maxModule) maxModule = width;
                if (span > maxSpan) maxSpan = span;
            }


        }

        /// <summary>
        /// Returns the difference between the original brief set and the current state
        /// </summary>
        /// <returns>String</returns>
        public string GetBriefDifference()
        {
            string data = "";

            Dictionary<string, float> areas = new Dictionary<string, float>();
            Dictionary<string, float> percentages = new Dictionary<string, float>();
            float totalPercentages = 0;
            float totalArea = 0;
            try
            {
                for (int i = 1; i < floors.Count; i++)
                {
                    foreach (var item in floors[i].percentages)
                    {
                        if (percentages.ContainsKey(item.Key))
                        {
                            percentages[item.Key] += item.Value;
                        }
                        else
                        {
                            percentages.Add(item.Key, item.Value);
                        }
                        totalPercentages += item.Value;
                    }


                    var floor = floors[i];
                    if (floor != null && floor.apartments != null)
                    {
                        for (int j = 0; j < floor.apartments.Count; j++)
                        {
                            if (areas.ContainsKey(floor.apartments[j].ApartmentType))
                            {
                                areas[floor.apartments[j].ApartmentType] += floor.apartments[j].Area * floors[i].levels.Length;
                            }
                            else
                            {
                                areas.Add(floor.apartments[j].ApartmentType, floor.apartments[j].Area * floors[i].levels.Length);
                            }
                            totalArea += floor.apartments[j].Area * floors[i].levels.Length;
                        }
                    }
                }

                if (areas.Count > 0)
                {
                    foreach (var item in percentages)
                    {
                        if (item.Key != "Commercial" && item.Key != "Other")
                        {
                            var perc = percentages[item.Key];
                            perc /= totalPercentages;
                            perc *= 100;
                            perc = (float)Math.Round(perc, 1);
                            var area = areas[item.Key];
                            area /= totalArea;
                            area *= 100;
                            area = (float)Math.Round(area, 1);
                            data += string.Format("{0} = Brief {1}% vs Current {2}%\n", item.Key, Math.Round(perc, 1), Math.Round(area, 1));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

            return data;
        }

        /// <summary>
        /// Returns the total plant area needed for the building
        /// </summary>
        /// <returns>Float</returns>
        public float GetBuildingPlantArea()
        {
            float area = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                area += (floors[i].GIA * floors[i].levels.Length) * 0.05f;
            }
            return area;
        }

        /// <summary>
        /// Returns the reception area required for the building
        /// </summary>
        /// <returns>Float</returns>
        public float GetBuildingReceptionArea()
        {
            float area = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                area += (floors[i].GIA * floors[i].levels.Length) * 0.01f;
            }
            return area;
        }

        /// <summary>
        /// Returns the area which is not assigned a specific use
        /// </summary>
        /// <param name="receptionArea">The reception area of the ground floor</param>
        /// <returns>Float</returns>
        public float GetOtherArea(float receptionArea)
        {
            float area = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                if (floors[i].percentages.ContainsKey("Other"))
                {
                    if (i == 0)
                    {
                        area += (floors[i].percentages["Other"] / 100f) * (floors[i].GIA - receptionArea);
                    }
                    else
                    {
                        area += (floors[i].percentages["Other"] / 100f) * floors[i].GIA;
                    }
                }
            }
            return area;
        }

        /// <summary>
        /// Returns the commercial area
        /// </summary>
        /// <param name="receptionArea">The reception area of the ground floor</param>
        /// <returns>Float</returns>
        public float GetCommercialArea(float receptionArea)
        {
            float area = 0;
            for (int i = 0; i < floors.Count; i++)
            {
                if (floors[i].percentages.ContainsKey("Commercial"))
                {
                    if (i == 0)
                    {
                        area += (floors[i].percentages["Commercial"] / 100f) * (floors[i].GIA - receptionArea);
                    }
                    else
                    {
                        area += (floors[i].percentages["Commercial"] / 100f) * floors[i].GIA;
                    }
                }
            }
            return area;
        }

        /// <summary>
        /// Offsets the centreline and creates a geometry for the floors of this building
        /// </summary>
        /// <param name="depth">The apartment depth</param>
        /// <param name="maxWidth">The maximum apartment width</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator Offset(float depth, float maxWidth)
        {
            prevMinimumWidth = maxWidth;
            plantRoomArea = 0;
            apartmentDepth = depth;
            for (int i = 0; i < floors.Count; i++)
            {
                var item = floors[i];

                if (!item.hasGeometry)
                {
                    if (i == 0)
                    {
                        var polygon = UseMasterPolyline(item.id);
                        if (item.centreLine == null)
                        {
                            SetCentreLineToFloor(item, polygon, i);
                        }
                        item.apartmentDepth = depth;
                        yield return StartCoroutine(item.GenerateGeometries(prevMinimumWidth));
                        buildingManager.apartmentTypesChart.designData = item;
                    }
                    else
                    {
                        var yVal = buildingElement.GetYForLevel(item.levels[0], i);
                        item.yVal = yVal;
                        item.transform.position = new Vector3(item.transform.position.x, yVal, item.transform.position.z);
                        var polygon = UseMasterPolyline(item.id);
                        polygon.transform.SetParent(transform);
                        polygon.gameObject.name = "CopyMaster_" + i;
                        if (item.centreLine == null)
                        {
                            SetCentreLineToFloor(item, polygon, i);
                        }
                        item.apartmentDepth = depth;
                        yield return StartCoroutine(item.GenerateGeometries(floors[0], prevMinimumWidth));
                        buildingManager.apartmentTypesChart.designData = item;
                    }
                }
                plantRoomArea += ((item.GIA * item.levels.Length) * 0.05f);
            }
            if (heightLine == null)
            {
                AddHeightLine(masterPolyline);
            }
            UpdateHeightLine(masterPolyline);

            if (previewMode == PreviewMode.Buildings)
                buildingManager.SetSelectedFloor(this, floors.Count - 1);

            floors[0].GeneratePlantRoom(plantRoomArea);
            buildingManager.CheckSystemConstraints(this);
        }

        /// <summary>
        /// Called when the preview mode changes
        /// </summary>
        /// <param name="mode">The current preview mode</param>
        public void OnPreviewModeChanged(PreviewMode mode)
        {
            this.previewMode = mode;
        }

        /// <summary>
        /// Sets the master polyline for this building
        /// </summary>
        /// <param name="polygon">The polygon to be used as the master one</param>
        public void SetMasterPolyline(Polygon polygon)
        {
            transform.position = polygon[0].currentPosition;
            polygon.transform.SetParent(transform);
            polygon.gameObject.name = "MasterPolyline_" + index;
            if (masterPolyline != null)
            {
                Destroy(masterPolyline);
                if (floors[0].centreLine != null)
                {
                    Destroy(floors[0].centreLine.gameObject);
                    floors[0].centreLine = null;
                }
                for (int i = 0; i < floors.Count; i++)
                {
                    if (floors[i].centreLine)
                    {
                        Destroy(floors[i].centreLine.gameObject);
                        floors[i].centreLine = null;
                    }
                }
            }
            masterPolyline = polygon;
            polygon.updated.AddListener(UpdateHeightLine);
        }

        /// <summary>
        /// Updates the line display of the total height of the building
        /// </summary>
        /// <param name="poly">The polygon which triggered this update</param>
        public void UpdateHeightLine(Polygon poly)
        {
            if (heightLine != null)
            {
                if (buildingType == BuildingType.Linear)
                {
                    if (poly != null)
                    {
                        heightLine.gameObject.SetActive(true);
                        float totalHeight = floors[0].floor2floor;
                        for (int i = 1; i < floors.Count; i++)
                        {
                            try
                            {
                                totalHeight += floors[i].levels.Length * floors[i].floor2floor;
                            }
                            catch (Exception e)
                            {
                                Debug.Log(e);
                            }
                        }
                        //For the transfer decks
                        totalHeight += (floors.Count - 1) * 0.5f;
                        Vector3 start = (poly[0].currentPosition + (poly[0].currentPosition - poly[1].currentPosition).normalized * offsetDistanceLine);
                        Vector3 end = start + Vector3.up * totalHeight;
                        UpdateDistance(start, end, heightLine);
                    }
                    else
                    {
                        heightLine.gameObject.SetActive(false);
                    }
                }
                else
                {
                    heightLine.gameObject.SetActive(true);
                    float totalHeight = floors[0].floor2floor;
                    for (int i = 1; i < floors.Count; i++)
                    {
                        try
                        {
                            totalHeight += floors[i].levels.Length * floors[i].floor2floor;
                        }
                        catch (Exception e)
                        {
                            Debug.Log(e);
                        }
                    }
                    //For the transfer decks
                    totalHeight += (floors.Count - 1) * 0.5f;
                    var corner = (floors[0] as ProceduralTower).exteriorPolygon[0].currentPosition;
                    Vector3 start = corner + (corner - floors[0].transform.position).normalized * offsetDistanceLine;
                    Vector3 end = start + Vector3.up * totalHeight;
                    UpdateDistance(start, end, heightLine);
                }
            }
        }

        /// <summary>
        /// Called to delete a floor of this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be deleted</param>
        public void DeleteFloor(int floorIndex)
        {
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                Destroy(currentFloor.gameObject);
                currentFloor = null;
                floors.RemoveAt(floorIndex);
            }
        }

        /// <summary>
        /// Called when this building (as a tower) has been placed
        /// </summary>
        public void PlaceTower()
        {
            Placed = true;
            if (buildingType == BuildingType.Tower)
            {
                for (int i = 0; i < floors.Count; i++)
                {
                    floors[i].SetPreviewMode(buildingManager.previewMode);
                }
                AddHeightLine();
                UpdateHeightLine(null);
                StartCoroutine(OnSelect());
            }
        }

        /// <summary>
        /// Adds a tower type floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="aptTypes">The list of apartment types for the added floor</param>
        /// <returns>Procedural Tower Floor</returns>
        public ProceduralTower AddTowerFloor(int floorIndex, string name, string[] aptTypes)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }

            ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
            m_tower.transform.localPosition += new Vector3(0, 5, 0);
            m_tower.building = this;
            m_tower.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_tower.apartmentDepth = 6.8f;
            currentFloor = m_tower;
            m_tower.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_tower;
            int[] levels = new int[(int)totalHeight];
            for (int i = 1; i <= levels.Length; i++)
            {
                levels[i - 1] = i;
            }
            currentFloor.levels = levels;
            m_tower.Initialize(aptTypes, currentCoreDimensions);
            floors.Add(currentFloor);
            return m_tower;
            //if (floors.Count == 1)
            //{
            //    AddTowerGroundFloor(0, name, configurationType, m_tower);
            //}
        }

        ///// <summary>
        ///// Adds a tower ground floor to this building
        ///// </summary>
        ///// <param name="floorIndex"></param>
        ///// <param name="name"></param>
        ///// <param name="configurationType"></param>
        ///// <param name="tower"></param>
        //public void AddTowerGroundFloor(int floorIndex, string name, string configurationType, ProceduralTower tower)
        //{
        //    if (floors == null)
        //    {
        //        floors = new List<ProceduralFloor>();
        //    }

        //    ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
        //    m_tower.floor2floor = 4.5f;
        //    m_tower.building = this;
        //    m_tower.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
        //    float aptDepth = 6.8f;
        //    float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
        //    m_tower.apartmentDepth = 6.8f;
        //    m_tower.key = Guid.NewGuid().ToString();
        //    lastAddedFloor = m_tower;
        //    int[] levels = new int[] { 0 };
        //    m_tower.levels = levels;
        //    m_tower.Initialize(tower);
        //    m_tower.transform.SetAsFirstSibling();
        //    floors.Insert(0, m_tower);
        //}

        /// <summary>
        /// Adds a floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor AddFloor(int floorIndex, string name)
        {
            if (buildingType == BuildingType.Linear)
            {
                if (floors == null)
                {
                    floors = new List<ProceduralFloor>();
                }
                if (floors.Count > floorIndex)
                {
                    currentFloor = floors[floorIndex];
                    Destroy(currentFloor.gameObject);
                    currentFloor = null;
                    floors.RemoveAt(floorIndex);
                }

                ProceduralFloor m_floor = Instantiate(proceduralFloorPrefab, transform).GetComponent<ProceduralFloor>();
                m_floor.transform.localPosition = Vector3.zero;
                m_floor.transform.localEulerAngles = Vector3.zero;
                m_floor.transform.localScale = Vector3.one;

                m_floor.Initialize();
                m_floor.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
                m_floor.building = this;
                float aptDepth = 6.8f;
                float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
                m_floor.apartmentDepth = 6.8f;
                currentFloor = m_floor;
                m_floor.key = Guid.NewGuid().ToString();//index + "," + floorIndex;
                lastAddedFloor = m_floor;

                floors.Add(currentFloor);
                return currentFloor;
            }
            else
            {
                return AddTowerFloor(floorIndex, name, new string[] { "2b4p", "2b4p", "1b2p", "2b4p", "2b4p", "1b2p" });
            }
        }

        /// <summary>
        /// Loads a floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="percentages">The percentages of the floor to be added</param>
        /// <param name="f2f">The floor to floor height of the floor to be added</param>
        /// <param name="levels">The levels of the floor to be added</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor LoadFloor(int floorIndex, string name, Dictionary<string, float> percentages, float f2f, int[] levels)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                Destroy(currentFloor.gameObject);
                currentFloor = null;
                floors.RemoveAt(floorIndex);
            }

            ProceduralFloor m_floor = Instantiate(proceduralFloorPrefab, transform).GetComponent<ProceduralFloor>();
            m_floor.transform.localPosition = Vector3.zero;
            m_floor.transform.localEulerAngles = Vector3.zero;
            m_floor.transform.localScale = Vector3.one;

            m_floor.Initialize();
            m_floor.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            //m_floor.id = floorIndex;
            m_floor.percentages = percentages;
            m_floor.floor2floor = f2f;
            m_floor.levels = levels;
            m_floor.building = this;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_floor.apartmentDepth = 6.8f;
            currentFloor = m_floor;
            m_floor.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_floor;

            floors.Add(currentFloor);
            return currentFloor;
        }

        /// <summary>
        /// Loads a tower floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="percentages">The percentages of the floor to be added</param>
        /// <param name="f2f">The floor to floor height of the floor to be added</param>
        /// <param name="levels">The levels of the floor to be added</param>
        /// <param name="aptTypes">The list of apartment types for the floor to be added</param>
        /// <returns>Procedural Tower</returns>
        public ProceduralTower LoadTowerFloor(int floorIndex, string name, Dictionary<string, float> percentages, float f2f, int[] levels, string[] aptTypes)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                Destroy(currentFloor.gameObject);
                currentFloor = null;
                floors.RemoveAt(floorIndex);
            }

            currentCoreDimensions = new Vector3(currentCoreDimensions.x, f2f, currentCoreDimensions.z);
            ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
            m_tower.transform.localPosition += new Vector3(0, buildingElement.GetYForLevel(), 0);
            m_tower.building = this;
            m_tower.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_tower.apartmentDepth = 6.8f;
            currentFloor = m_tower;
            m_tower.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_tower;
            currentFloor.levels = levels;
            m_tower.Initialize(aptTypes, currentCoreDimensions);
            m_tower.percentages = percentages;
            floors.Add(currentFloor);
            return m_tower;
        }

        /// <summary>
        /// Adds a tower floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="levels">The levels of the floor to be added</param>
        /// <param name="f2f">The floor to floor height of the floor to be added</param>
        /// <param name="aptTypes">The list of apartment types for the floor to be added</param>
        /// <returns>Procedural Tower</returns>
        public ProceduralTower AddTowerFloor(int floorIndex, string name, int[] levels, float f2f, string[] aptTypes)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }
            currentCoreDimensions = new Vector3(currentCoreDimensions.x, f2f, currentCoreDimensions.z);
            ProceduralTower m_tower = Instantiate(proceduralTowerPrefab, transform).GetComponent<ProceduralTower>();
            m_tower.transform.localPosition += new Vector3(0, buildingElement.GetYForLevel(), 0);
            m_tower.building = this;
            m_tower.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_tower.apartmentDepth = 6.8f;
            currentFloor = m_tower;
            m_tower.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_tower;
            currentFloor.levels = levels;
            m_tower.Initialize(aptTypes, currentCoreDimensions);
            m_tower.percentages = FloorElement.GetTowerPercentages(aptTypes);
            m_tower.floor2floor = f2f;
            floors.Add(currentFloor);
            return m_tower;
        }

        /// <summary>
        /// Adds a floor to this building
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be added</param>
        /// <param name="name">The name of the floor to be added</param>
        /// <param name="levels">The levels of the floor to be added</param>
        /// <param name="f2f">The floor to floor height of the floor to be added</param>
        /// <param name="percentages">The percentages of the floor to be added</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor AddFloor(int floorIndex, string name, int[] levels, float f2f, Dictionary<string, float> percentages)
        {
            if (floors == null)
            {
                floors = new List<ProceduralFloor>();
            }
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                Destroy(currentFloor.gameObject);
                currentFloor = null;
                floors.RemoveAt(floorIndex);
            }

            ProceduralFloor m_floor = Instantiate(proceduralFloorPrefab, transform).GetComponent<ProceduralFloor>();
            m_floor.transform.localPosition = Vector3.zero;
            m_floor.transform.localEulerAngles = Vector3.zero;
            m_floor.transform.localScale = Vector3.one;
            m_floor.gameObject.name = name + ":" + index + "_Floor_" + floorIndex;
            //m_floor.id = floorIndex;
            //m_floor.transform.position = new Vector3(0, buildingManager.designInputTabs.GetCurrentYValue(), 0);
            m_floor.Initialize();
            m_floor.levels = levels;
            m_floor.percentages = percentages;
            m_floor.floor2floor = f2f;
            //m_floor.buildingManager = buildingManager;
            m_floor.building = this;
            float aptDepth = 6.8f;
            float.TryParse(buildingManager.offsetInputField.text, out aptDepth);
            m_floor.apartmentDepth = 6.8f;
            currentFloor = m_floor;
            m_floor.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_floor;

            floors.Add(currentFloor);
            return currentFloor;
        }

        /// <summary>
        /// Sets the selected floor for this building
        /// </summary>
        /// <param name="floorIndex">The index of the selected floor</param>
        public void SetCurrentFloor(int floorIndex)
        {
            currentFloor = floors[floorIndex];
        }

        /// <summary>
        /// Adds a floor with a custom polygon to this building
        /// </summary>
        /// <param name="offsetDistance">The offset distance for the floor's centreline</param>
        /// <param name="polygon">The custom polygon</param>
        public void AddCustomFloorPolygon(float offsetDistance, PolygonSegment polygon)
        {
            int floorIndex = floors.IndexOf(currentFloor);
            if (floors.Count > floorIndex)
            {
                currentFloor = floors[floorIndex];
                if (currentFloor.centreLine != null)
                {
                    Destroy(currentFloor.centreLine.gameObject);
                }
            }

            ProceduralFloor m_floor = currentFloor;
            m_floor.transform.position = new Vector3(0, buildingElement.GetYForLevel(floorIndex), 0);
            polygon.transform.SetParent(m_floor.transform);
            polygon.gameObject.name = "CustomPolygon_" + floorIndex;
            m_floor.building = this;
            m_floor.Initialize(polygon, true);
            //m_floor.id = floorIndex;
            //m_floor.key = Guid.NewGuid().ToString();
            lastAddedFloor = m_floor;

            List<float> vertices = new List<float>();
            for (int i = 0; i < currentFloor.centreLine.Count; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    vertices.Add(currentFloor.centreLine[i].currentPosition[j]);
                }
            }

            if (m_floor.hasGeometry)
            {
                StartCoroutine(m_floor.GenerateGeometries(currentMaximumWidth));
            }
        }

        /// <summary>
        /// Loads the geometries for a floor of this building
        /// </summary>
        /// <param name="depth">The apartment depth</param>
        /// <param name="floor2floor">The floor to floor height</param>
        /// <param name="state">The save state of the floor</param>
        public void LoadGeometries(float depth, float floor2floor, FloorLayoutState state)
        {
            currentFloor = floors[state.floorIndex];
            currentFloor.apartmentDepth = depth;
            apartmentDepth = depth;
            currentFloor.floor2floor = floor2floor;
            if (!state.isTower)
            {
                if (state.isCopy)
                {
                    SetCentreLineToFloor(currentFloor, UseMasterPolyline(state.floorIndex), state.floorIndex);
                    currentFloor.projectedCores = true;
                    currentFloor.LoadGeometries(state, floors[0]);
                }
                else if (state.isCustom)
                {
                    currentFloor.projectedCores = true;
                    currentFloor.LoadGeometries(state, floors[0]);
                }
                else
                {
                    SetCentreLineToFloor(currentFloor, UseMasterPolyline(state.floorIndex), state.floorIndex);
                    currentFloor.LoadGeometries(state, null);
                }
                if (heightLine == null && masterPolyline != null)
                {
                    AddHeightLine(masterPolyline);
                    UpdateHeightLine(masterPolyline);
                }
            }
            else
            {
                if (state.floorIndex == 0)
                {
                    currentFloor.LoadGeometries(state, floors[1]);
                }
                else
                {
                    currentFloor.LoadGeometries(state, null);
                }
            }
        }

        /// <summary>
        /// Called when the floor to floor height or the levels of a floor have changed
        /// </summary>
        public void OnFloorHeightChanged()
        {
            UpdateHeightLine(masterPolyline);
            floors[0].UpdatePlantRoom();
            buildingManager.CheckSystemConstraints(this);
            StartCoroutine(buildingManager.OverallUpdate());
        }

        /// <summary>
        /// Adds a height line to this building
        /// </summary>
        /// <param name="poly">The polygon from which the heightline will get its position</param>
        public void AddHeightLine(Polygon poly)
        {
            if (heightLine != null)
            {
                Destroy(heightLine);
                heightLine = null;
            }

            currentDistance = new GameObject("Distance_" + index);
            heightLine = currentDistance;
            currentDistance.transform.SetParent(transform);

            GameObject text = Instantiate(Resources.Load("Geometry/TextMesh") as GameObject, currentDistance.transform);
            text.transform.Rotate(text.transform.right, 90);
            TextMesh mText = text.GetComponent<TextMesh>();
            mText.characterSize = 1;

            if (useCanvas)
            {
                mText.GetComponent<MeshRenderer>().enabled = false;
                GameObject distUI = Instantiate(Resources.Load("GUI/DistanceText") as GameObject, distanceUIParent);
                distanceUI = distUI.GetComponent<Text>();
                distUI.GetComponent<Text>().color = distancesMaterial.color;
                distUI.GetComponent<UiRepresentation>().Initialize(text.transform);
            }

            GameObject line1 = new GameObject("Line1");
            line1.transform.SetParent(currentDistance.transform);
            LineRenderer l1 = line1.AddComponent<LineRenderer>();
            l1.material = distancesMaterial;
            l1.useWorldSpace = true;
            l1.positionCount = 2;
            l1.startWidth = lineWidth;
            l1.endWidth = lineWidth;
            l1.SetPosition(0, (poly[0].currentPosition + (poly[0].currentPosition - poly[1].currentPosition).normalized * offsetDistanceLine));
            l1.SetPosition(1, (poly[0].currentPosition + (poly[0].currentPosition - poly[1].currentPosition).normalized * offsetDistanceLine));
        }

        /// <summary>
        /// Adds a height line to the building
        /// </summary>
        public void AddHeightLine()
        {
            if (heightLine != null)
            {
                Destroy(heightLine);
                heightLine = null;
            }

            currentDistance = new GameObject("Distance_" + index);
            heightLine = currentDistance;
            currentDistance.transform.SetParent(transform);
            currentDistance.transform.position = transform.position;

            GameObject text = Instantiate(Resources.Load("Geometry/TextMesh") as GameObject, currentDistance.transform);
            text.transform.Rotate(text.transform.right, 90);
            TextMesh mText = text.GetComponent<TextMesh>();
            mText.characterSize = 1;

            if (useCanvas)
            {
                mText.GetComponent<MeshRenderer>().enabled = false;
                GameObject distUI = Instantiate(Resources.Load("GUI/DistanceText") as GameObject, distanceUIParent);
                distanceUI = distUI.GetComponent<Text>();
                distUI.GetComponent<Text>().color = distancesMaterial.color;
                distUI.GetComponent<UiRepresentation>().Initialize(text.transform);
            }

            GameObject line1 = new GameObject("Line1");
            line1.transform.SetParent(currentDistance.transform);
            LineRenderer l1 = line1.AddComponent<LineRenderer>();
            l1.material = distancesMaterial;
            l1.useWorldSpace = true;
            l1.positionCount = 2;
            l1.startWidth = lineWidth;
            l1.endWidth = lineWidth;
            var corner = floors[0].exteriorPolygon[0].currentPosition;
            l1.SetPosition(0, (corner + (corner - floors[0].transform.position).normalized * offsetDistanceLine));
            l1.SetPosition(1, (corner + (corner - floors[0].transform.position).normalized * offsetDistanceLine));
        }
        #endregion

        #region Private Methods
        private IEnumerator OnSelect()
        {
            yield return StartCoroutine(buildingManager.OnBuildingSelected(this));
            buildingManager.SetSelectedFloor(this, floors.IndexOf(currentFloor));
            if (previewMode == PreviewMode.Floors)
            {
                if (currentFloor.centreLine != null)
                {
                    currentFloor.centreLine.gameObject.SetActive(true);
                }
                currentFloor.ToggleChildren(true);
            }
        }

        private void UpdateDistance(Vector3 start, Vector3 end, GameObject distance)
        {
            float dist = Vector3.Distance(start, end);

            TextMesh mText = distance.transform.GetChild(0).GetComponent<TextMesh>();
            mText.text = dist.ToString("0.00");
            distance.transform.GetChild(0).position = start + (end - start) / 2 + Vector3.Cross((end - start), Vector3.right).normalized * distanceOffset;

            if (useCanvas)
            {
                distanceUI.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(distance.transform.GetChild(0).position);
                distanceUI.text = dist.ToString("0.00");
            }

            float angle = Vector3.Angle(Vector3.right, end - start);
            if (Vector3.Cross((end - start), Vector3.right).y < 0)
            {
                angle = -angle;
            }
            distance.transform.GetChild(0).eulerAngles = new Vector3(90, -angle, 0);

            LineRenderer l1 = distance.transform.GetChild(1).GetComponent<LineRenderer>();
            l1.SetPosition(0, start);
            l1.SetPosition(1, end);
        }



        private void SetCentreLineToFloor(ProceduralFloor m_floor, Polygon polygon, int key)
        {
            m_floor.Initialize(polygon);
            polygon.transform.SetParent(m_floor.transform);
            List<float> vertices = new List<float>();
            for (int i = 0; i < m_floor.centreLine.Count; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    vertices.Add(m_floor.centreLine[i].currentPosition[j]);
                }
            }
        }

        private Polygon UseMasterPolyline(int index)
        {
            if (index - 1 >= 0)
            {
                float yValue = buildingElement.GetYForLevel(index);
                Polygon toBeCopied = masterPolyline;
                return buildingManager.polygonDrawer.CopyPolygon(toBeCopied, yValue);
            }
            else return masterPolyline;
        }
        #endregion
    }
}
