﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour which has all the static Standards and General Information
    /// </summary>
    public class Standards : Tagged<Standards>
    {
        /// <summary>
        /// Unity event for when the apartment area ranges have changed
        /// </summary>
        public OnAptAreasChanged onAptsAreasChanged;
        /// <summary>
        /// The procedural building manager of the project
        /// </summary>
        public ProceduralBuildingManager buildingManager;
        /// <summary>
        /// The UI element which handles the construction features
        /// </summary>
        public WallWidth constructionFeaturesElement;
        /// <summary>
        /// The dropdown for the type of amenities in the project
        /// </summary>
        public Dropdown balconiesDropdown;
        /// <summary>
        /// The type of amenities in the project
        /// </summary>
        public Balconies balconies = Balconies.External;

        /// <summary>
        /// The required structural wall dimensions based on height
        /// </summary>
        public float[] structuralWallDimensions;
        /// <summary>
        /// The minimum structural wall thickness in the project
        /// </summary>
        public float minStructuralWallWidth;
        /// <summary>
        /// The different heights for the tower in order to calculate the wall thicknesses and core sizes
        /// </summary>
        public int[] towerHeightsForCores;
        /// <summary>
        /// The required core sizes based on height
        /// </summary>
        public Vector3[] coreDimensions;


        /// <summary>
        /// Evaluates the wall thicknesses against the required thickness for the structural walls
        /// </summary>
        public void EvaluateStructuralWalls()
        {
            float maxHeight = float.MinValue;

            for (int i = 0; i < buildingManager.proceduralBuildings.Count; i++)
            {
                if (buildingManager.proceduralBuildings[i].buildingType == BuildingType.Tower)
                    if (maxHeight < buildingManager.proceduralBuildings[i].maxLevel)
                        maxHeight = buildingManager.proceduralBuildings[i].maxLevel;
            }

            float requiredWall = 0.3f;

            if (maxHeight < towerHeightsForCores[0])
            {
                requiredWall = structuralWallDimensions[0];
            }
            else if (maxHeight >= towerHeightsForCores[0] && maxHeight < towerHeightsForCores[1])
            {
                requiredWall = structuralWallDimensions[1];
            }
            else
            {
                requiredWall = structuralWallDimensions[2];
            }

            if (requiredWall > ConstructionFeatures["PartyWall"])
            {
                if (!Notifications.TaggedObject.activeNotifications.Contains("TowerWall"))
                {
                    Notifications.TaggedObject.activeNotifications.Add("TowerWall");
                }
            }
            else
            {
                if (Notifications.TaggedObject.activeNotifications.Contains("TowerWall"))
                {
                    Notifications.TaggedObject.activeNotifications.Remove("TowerWall");
                }
            }
            Notifications.TaggedObject.UpdateNotifications();
        }

        /// <summary>
        /// The widths of the different walls
        /// </summary>
        public Dictionary<string, float> ConstructionFeatures = new Dictionary<string, float>()
        {
            {"PartyWall",0.3f },
            {"ExteriorWall", 0.45f },
            {"CorridorWall", 0.25f },
            {"PanelLength",10.5f },
            {"LineSpan", 6.5f },
        };

        /// <summary>
        /// The exxpanded names of the construction features (for reports)
        /// </summary>
        public Dictionary<string, string> ConstructionFeaturesNames = new Dictionary<string, string>()
        {
            {"PartyWall","Party Wall" },
            {"ExteriorWall", "Exterior Wall" },
            {"CorridorWall", "Corridor Wall" },
            {"PanelLength","Maximum Panel Length" },
            {"LineSpan", "Maximum Line-Load Span" },
        };

        /// <summary>
        /// The dictionary of the construction features for the report
        /// </summary>
        public Dictionary<string, float> ConstructionFeaturesReport
        {
            get
            {
                Dictionary<string, float> _dict = new Dictionary<string, float>();

                foreach(var constr in ConstructionFeatures)
                {
                    _dict.Add(ConstructionFeaturesNames[constr.Key], constr.Value);
                }

                return _dict;
            }
        }

        /// <summary>
        /// A dictionary with the minimum size per apartment type (excluding amenities areas)
        /// </summary>
        public Dictionary<string, double> apartmentTypesMinimumSizes = new Dictionary<string, double>()
        {
            {"Studio",37 },
            {"1b2p",50 },
            {"2b3p",61 },
            {"2b4p",70 },
            {"3b5p",86 },
            {"SpecialApt",10 }
        };

        /// <summary>
        /// The amenities areas as stored in run-time
        /// </summary>
        public Dictionary<string, float> amenitiesAreas = new Dictionary<string, float>()
        {
            {"Studio",0 },
            {"1b2p",0 },
            {"2b3p",0 },
            {"2b4p",0 },
            {"3b5p",0 },
            {"SpecialApt",0 }
        };

        /// <summary>
        /// The assigned areas for each of the apartment types
        /// </summary>
        public Dictionary<string, double> DesiredAreas
        {
            get
            {
                if (balconies == Balconies.External)
                {
                    return desiredAreas;
                }
                else
                {
                    Dictionary<string, double> temp = new Dictionary<string, double>();
                    foreach (var item in desiredAreas)
                    {
                        temp.Add(item.Key, item.Value + amenitiesAreas[item.Key]);
                    }
                    return temp;
                }
            }
            set
            {
                desiredAreas = value;
            }
        }

        
        private Dictionary<string, double> desiredAreas = new Dictionary<string, double>()
        {
            {"Studio",37 },
            {"1b2p",50 },
            {"2b3p",61 },
            {"2b4p",70 },
            {"3b5p",86 },
            {"SpecialApt",10 }
        };

        /// <summary>
        /// A dictionary with the minimum size per apartment type (including amenities areas)
        /// </summary>
        public Dictionary<string, double> ApartmentTypesMinimumSizes
        {
            get
            {
                if (balconies == Balconies.External)
                {
                    return apartmentTypesMinimumSizes;
                }
                else
                {
                    Dictionary<string, double> temp = new Dictionary<string, double>();
                    foreach (var item in apartmentTypesMinimumSizes)
                    {
                        temp.Add(item.Key, item.Value + amenitiesAreas[item.Key]);
                    }
                    return temp;
                }
            }
        }

        /// <summary>
        /// A dictionary with the maximum size per apartment type
        /// </summary>
        public Dictionary<string, double> ApartmentTypesMaximumSizes = new Dictionary<string, double>()
        {
            {"SpecialApt",10 },
            {"Studio",41 },
            {"1b2p",55 },
            {"2b3p",67 },
            {"2b4p",77 },
            {"3b5p",97 },
        };

        /// <summary>
        /// The number of habitable rooms per apartment type
        /// </summary>
        public Dictionary<string, int> HabitableRoomsPerApartment = new Dictionary<string, int>()
        {
            {"SpecialApt",0 },
            {"Studio", 1 },
            {"1b2p",2 },
            {"2b3p",3 },
            {"2b4p",3 },
            {"3b5p",4 },
        };

        /// <summary>
        /// Presets of wall widths for different manufacturing systems
        /// </summary>
        public Dictionary<string, double[]> ManufacturingPresets = new Dictionary<string, double[]>()
        {
            //Exterior, Party, Corridor, PanelLength, LineSpan
            {"Traditional", new double[] {0.45,0.3,0.25,10.5f,6.5f} },
            {"HRS_Modules", new double[] { 0.413, 0.27, 0.27, 10.5f, 6.5f } },
            {"CLT_Modules", new double[] { 0.453, 0.43, 0.43, 10.5f, 6.5f } },
            {"Steel_Panels", new double[] { 0.475, 0.294, 0.294, 8, 4.6f } },
            {"Timber_Panels", new double[] { 0.453, 0.39, 0.39, 6, 4.6f } },
            {"CLT_Panels", new double[] { 0.453, 0.3, 0.3, 13, 6 } },
            {"Concrete_Panels", new double[] { 0.52, 0.35, 0.35, 8, 8.5 } }
        };

        /// <summary>
        /// Standard colours for each apartment type
        /// </summary>
        public Dictionary<string, Color> ApartmentTypesColours = new Dictionary<string, Color>()
        {
            {"SpecialApt",Color.grey },
            {"Studio",new Color(103/255.0f,12/255.0f,61/255.0f)},
            {"1b2p",new Color(123/255.0f,32/255.0f,81/255.0f)},
            {"2b3p", new Color(229/255.0f,217/255.0f,223/255.0f) },
            {"2b4p", new Color(202/255.0f,174/255.0f,188/255.0f) },
            {"3b5p", new Color(164/255.0f,105/255.0f,136/255.0f) },
        };

        /// <summary>
        /// The indices of the vertices to be used for the dimensions
        /// int[0]: start
        /// int[1]: end
        /// int[2]: side of apartment to display the dimension (0:left, 1:under, 2:right, 3:above)
        /// </summary>
        public Dictionary<string, int[]> RoomsDimensionIndices = new Dictionary<string, int[]>()
    {
        {"BA" , new int[3] {0,1,1}},
        {"DB" , new int[3] {5,4,3}},
        {"PB" , new int[3] {5,4,3}},
        {"PBB" , new int[3] {0,1,3}},
        {"DBB" , new int[3] {0,1,3}},
        {"HW" , new int[3] {0,1,1}},
        {"ST" , new int[3] {0,1,1}},
        {"STT" , new int[3] {0,1,1}},
        {"HWW" , new int[3] {0,1,1}},
        {"BAA" , new int[3] {0,1,1}},
        {"WC" , new int[3] {0,1,1}},
        {"WCC" , new int[3] {0,1,1}},
        {"PSB" , new int[3] {0,1,3}},
        {"ADB" , new int[3] {0,1,3}},
        {"ADBB" , new int[3] {0,1,3}},
        {"APB" , new int[3] {0,1,3}},
        {"APBB" , new int[3] {0,1,3}},
        {"APSB" , new int[3] {0,1,3}},
        {"LR" , new int[3] {3,2,3}},
        {"LRR" , new int[3] {3,2,3}},
        {"ALR" , new int[3] {0,1,3}},
        {"ALRR" , new int[3] {0,1,3}},
        {"AILR" , new int[3] {0,1,3}},
        {"AILRR" , new int[3] {0,1,3}},
    };

        /// <summary>
        /// The minimum widths for the different rooms
        /// </summary>
        public Dictionary<string, float> MinimumRoomWidths = new Dictionary<string, float>()
    {
        {"BA" , 2},
        {"DB" , 2.75f},
        {"PB" , 2.75f},
        {"PBB" , 2.55f},
        {"DBB" , 2.15f},
        {"HW" , 0.9f},
        {"HWW" , 0.9f},
        {"BAA" , 2},
        {"PSB" , 2.15f},
        {"LR" , 3.2f},
        {"LRR" ,3.2f},
        {"WC" , 1f},
        {"WCC" ,1f},
    };

        /// <summary>
        /// The minimum areas for the different rooms
        /// </summary>
        public Dictionary<string, Dictionary<string, float>> MinimumRoomAreas = new Dictionary<string, Dictionary<string, float>>()
    {
            {"Studio", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 1.5f},
                    {"STT" , 1.5f},
                    {"ALR" , 5},
                    {"ALRR" , 5},
                    {"AILR" , 5},
                    {"AILRR" , 5},
                }
            },
            { "1b2p", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 1.5f},
                    {"STT" , 1.5f},
                    {"ALR" , 5},
                    {"ALRR" , 5},
                    {"AILR" , 5},
                    {"AILRR" , 5},
                }
            },
            {"2b3p", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 2f},
                    {"STT" , 2f},
                    {"ALR" , 6},
                    {"ALRR" , 6},
                    {"AILR" , 6},
                    {"AILRR" , 6},
                }
            },
            {"2b4p", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 2f},
                    {"STT" , 2f},
                    {"ALR" , 7},
                    {"ALRR" , 7},
                    {"AILR" , 7},
                    {"AILRR" , 7},
                }
            },
            {"3b5p", new Dictionary<string, float>()
                {
                    {"DB" , 11.5f},
                    {"PB" , 11.5f},
                    {"PBB" , 11.5f},
                    {"DBB" , 7.5f},
                    {"PSB" , 7.5f},
                    {"ST" , 2.5f},
                    {"STT" , 2.5f},
                    {"ALR" , 8},
                    {"ALRR" , 8},
                    {"AILR" , 8},
                    {"AILRR" , 8},
                }
            },
    };

        /// <summary>
        /// Which apartments should display their dimensions
        /// </summary>
        public Dictionary<string, bool> RoomDimensionBools = new Dictionary<string, bool>()
    {
        {"BA" , true},
        {"DB" , true},
        {"PB" , true},
        {"PBB" , true},
        {"DBB" , true},
        {"HW" , true},
        {"ST" , true},
        {"STT" , true},
        {"HWW" , true},
        {"BAA" , true},
        {"WC" , true},
        {"WCC" , true},
        {"PSB" , true},
        {"ADB" , false},
        {"ADBB" , false},
        {"APB" , false},
        {"APBB" , false},
        {"APSB" , false},
        {"LR" , true},
        {"LRR" , true},
        {"ALR" , false},
        {"ALRR" , false},
        {"AILR" , false},
        {"AILRR" , false},
    };

        /// <summary>
        /// The full names of each room type
        /// </summary>
        public Dictionary<string, string> RoomNames = new Dictionary<string, string>()
    {
        {"BA" , "Bathroom"},
        {"DB" , "Double Bedroom"},
        {"PB" , "Principal Bedroom"},
        {"PBB" , "Double Bedroom"},
        {"DBB" , "Single Bedroom"},
        {"HW" , "Hall"},
        {"ST" , "Storage"},
        {"STT" , "Storage"},
        {"HWW" , "Hall"},
        {"BAA" , "Bathroom"},
        {"WC" , "WC"},
        {"WCC" , "WC"},
        {"PSB" , "Single Bedroom"},
        {"ADB" , "Amenity"},
        {"ADBB" , "Amenity"},
        {"APB" , "Amenity"},
        {"APBB" , "Amenity"},
        {"APSB" , "Amenity"},
        {"LR" , "Kitchen/Living/Dining"},
        {"LRR" , "Kitchen/Living/Dining"},
        {"ALR" , "Amenity"},
        {"ALRR" , "Amenity"},
        {"AILR" , "Amenity"},
        {"AILRR" , "Amenity"},
    };

        /// <summary>
        /// The modules indices per apartment type
        /// </summary>
        public Dictionary<string, int[]> ModulesPerType = new Dictionary<string, int[]>()
        {
            {"Studio",new int[] {0} },
            {"1b2p", new int[] {1} },
            {"2b3p", new int[] {1,4} },
            {"2b4p", new int[] {2,3} },
            {"3b5p", new int[] {2,3,12} }
        };

        /// <summary>
        /// The Line-Load indices per apartment types
        /// </summary>
        public Dictionary<string, string[]> LineLoadsPerType = new Dictionary<string, string[]>()
        {
            {"Studio",new string[] {"BA"} },
            {"1b2p", new string[] {"BA","DB", "LR" } },
            {"2b3p", new string[] { "BA", "DB","DBB", "LR" } },
            {"2b4p", new string[] { "PB", "PBB", "STT", "LRR" } },
            {"3b5p", new string[] { "PB", "PBB", "PSB", "STT","BAA", "LRR" } }
        };

        /// <summary>
        /// The starting point of the line loads of each room
        /// </summary>
        public Dictionary<string, int[][]> lineLoadStart = new Dictionary<string, int[][]>()
    {
        {"BA" , new int[][] { new int[] { 1, 0 } } },
        {"DB" , new int[][] { new int[] { 3, 1 } }},
        {"PB" , new int[][] { new int[] { 3, 1 } }},
        {"PBB" , new int[][] { new int[] { 1, 1 } }},
        {"DBB" , new int[][] { new int[] { 1, 1 } }},
        {"BAA" , new int[][] { new int[] { 1, 0 } } },
        {"STT" , new int[][] { new int[] { 1, 0 } } },
        {"PSB" , new int[][] { new int[] { 1, 1 } }},
        {"LR" , new int[][] { new int[] { 5, 0 } }},
        {"LRR" , new int[][] { new int[] { 5, 0 } }},

    };

        /// <summary>
        /// The colors for each of the different module types
        /// </summary>
        public Dictionary<string, Color> ModuleTypeColours = new Dictionary<string, Color>()
    {
        {"DBModule",/*new Color(1,0.5f,0)*/ new Color(233/255.0f,179/255.0f,77/255.0f)},
        {"PBModule", /*new Color(1,0.5f,0)*/ new Color(233/255.0f,179/255.0f,77/255.0f)},
        {"DBBModule", /*Color.blue */ new Color(108/255.0f,164/255.0f,166/255.0f)},
        {"PSBModule", /*Color.blue */new Color(108/255.0f,164/255.0f,166/255.0f)},
        {"PBBModule", /*Color.cyan*/ new Color(240/255.0f,211/255.0f,133/255.0f) },
        {"LivingRoomModule", /*Color.grey*/ new Color(158/255.0f,201/255.0f,203/255.0f) },
        {"Not Modular", Color.gray }
    };

        /// <summary>
        /// The list of rooms for each apartment type
        /// </summary>
        public Dictionary<string, bool[]> ApartmentTypeRooms = new Dictionary<string, bool[]>()
        {
            {"SpecialApt", new bool[] { } },
            {"Studio", new bool[] { true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false } },
            {"1b2p", new bool[] { true,true,false,false, false, true,true, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false}},
            {"2b3p", new bool[] { true, true, false, false, true, true, true, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false } },
            {"2b4p", new bool[] { true, false, true, true, false, false, false, true, true, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false } },
            {"3b5p", new bool[] { true,false,true,true, false, false, false, true,true,true, false, true,true, false, false, false, false, false, false, true, false, false, false, false }},
        };

        /// <summary>
        /// The minimum sizes for each room
        /// </summary>
        public Dictionary<string, double> RoomTypesMinimumSizes = new Dictionary<string, double>()
    {
        { "DoubleBedroom", 11.50 },
        {"Bathroom", 4.30 },
        {"LivingRoom", 16.34 }
    };

        /// <summary>
        /// The list of rooms which have amenities and the index of the corresponding amenities
        /// </summary>
        public Dictionary<string, Dictionary<int, int>> amenitiesIndices = new Dictionary<string, Dictionary<int, int>>()
    {
        {"Exterior" ,//new int[] { 2, 13,5,14,3,15,4,16,12,17,20,22,21,23} },
            new Dictionary<int, int>()
            {
                { 1,13 },
                { 4,14 },
                { 2,15 },
                { 3,16 },
                { 12,17 },
                { 18,20 },
                { 19,21 }
            }
        },
        {"Interior" ,
            new Dictionary<int, int>()
            {
                { 18,22 },
                { 19,23 }
            }
        }//new int[] { 20,18,21,19} }
    };

        /// <summary>
        /// The colours of the rooms
        /// </summary>
        public Dictionary<string, Color> RoomTypesColors = new Dictionary<string, Color>()
    {
        { "DoubleBedroom", Color.cyan},
        {"Bathroom", Color.yellow },
        {"LivingRoom", new Color(253/255.0f,106/255.0f,2/255.0f) },
    };

        /// <summary>
        /// A list of all the valid load lines
        /// </summary>
        public Dictionary<string, List<string>> ValidLineLoads = new Dictionary<string, List<string>>()
        {

        };

        /// <summary>
        /// A list of all the panels materials
        /// </summary>
        public Dictionary<string, Material> panelsMaterials = new Dictionary<string, Material>();

        /// <summary>
        /// A list of all the modules materials
        /// </summary>
        public Dictionary<string, Material> modulesMaterials = new Dictionary<string, Material>();

        /// <summary>
        /// The minimum areas for amenities per apartment type
        /// </summary>
        public Dictionary<string, float> MinimumAmenitiesArea = new Dictionary<string, float>()
        {
            {"Studio",5f },
            {"1b2p",5f },
            {"2b3p",6f },
            {"2b4p",7f },
            {"3b5p",8f },
            {"SpecialApt",0 }
        };

        /// <summary>
        /// The total amenities areas per apartment type
        /// </summary>
        public Dictionary<string, Dictionary<string, float>> TotalAmenitiesArea = new Dictionary<string, Dictionary<string, float>>()
        {
            {"Studio", new Dictionary<string, float>()
                {
                    { "ALR",0 },
                    {"AILR",0 }
                }
            },
            {"1b2p", new Dictionary<string, float>()
                {
                    { "ADB",0 },
                    { "ALR",0 },
                    {"AILR",0 }
                }
            },
            {"2b3p", new Dictionary<string, float>()
                {
                    { "ADBB",0 },
                    { "ADB",0 },
                    { "ALR",0 },
                    {"AILR",0 }
                }
            },
            {"2b4p", new Dictionary<string, float>()
                {
                    { "APBB",0 },
                    { "APB",0 },
                    { "ALRR",0 },
                    {"AILRR",0 }
                }
            },
            {"3b5p", new Dictionary<string, float>()
                {
                    { "APBB",0 },
                    { "APB",0 },
                    {"APSB",0 },
                    { "ALRR",0 },
                    {"AILRR",0 }
                }
            },
        };

        /// <summary>
        /// Evaluates the Amenities areas for each apartment type
        /// </summary>
        public void EvaluateAmenitiesArea()
        {

            List<string> aptTypes = new List<string>();

            foreach (var apt in TotalAmenitiesArea)
            {
                var totalArea = 0f;
                foreach (var room in apt.Value)
                {
                    totalArea += room.Value;
                }
                if (totalArea < MinimumAmenitiesArea[apt.Key])
                {
                    aptTypes.Add(apt.Key);
                }
            }

            if (aptTypes.Count > 0)
            {
                Notifications.TaggedObject.SetNotificationBody("Amenity", string.Format("Amenity in apartments {0} is too small!", String.Join(", ", aptTypes.ToArray())));
                if (!Notifications.TaggedObject.activeNotifications.Contains("Amenity"))
                {
                    Notifications.TaggedObject.activeNotifications.Add("Amenity");
                }
            }
            else
            {
                if (Notifications.TaggedObject.activeNotifications.Contains("Amenity"))
                {
                    Notifications.TaggedObject.activeNotifications.Remove("Amenity");
                }
            }
        }

        /// <summary>
        /// The maximum density per PTAL zone
        /// </summary>
        public Dictionary<string, float> ptalZones = new Dictionary<string, float>()
        {
            {"0",  110},
            {"1a",110 },
            {"1b",110 },
            {"2", 240 },
            {"3",240 },
            {"4",405 },
            {"5",405 },
            {"6a",405 },
            {"6b",405 }
        };

        /// <summary>
        /// Evaluates the widths of the different rooms
        /// </summary>
        public void EvaluateStandardRooms()
        {
            string[] _roomNames = new string[] { "DB", "PB", "BA", "BAA", "WC", "WCC", "DBB", "PBB", "PSB" };
            string[] aptTypes = customVals.Keys.ToArray();
            for (int i = 0; i < customVals["Studio"].Length; i++)
            {
                bool widthIssue = false;
                List<string> aptsWithIssues = new List<string>();
                for (int j = 0; j < aptTypes.Length; j++)
                {
                    if (customVals[aptTypes[j]][i] < MinimumRoomWidths[_roomNames[i]])
                    {
                        widthIssue = true;
                        aptsWithIssues.Add(aptTypes[j]);
                    }
                }

                if (widthIssue)
                {
                    Notifications.TaggedObject.SetNotificationBody(_roomNames[i], string.Format("The {0} in apartments {1} is too narrow!", RoomNames[_roomNames[i]], String.Join(", ", aptsWithIssues.ToArray())));
                    if (!Notifications.TaggedObject.activeNotifications.Contains(_roomNames[i]))
                    {
                        Notifications.TaggedObject.activeNotifications.Add(_roomNames[i]);
                    }
                }
                else
                {
                    if (Notifications.TaggedObject.activeNotifications.Contains(_roomNames[i]))
                    {
                        Notifications.TaggedObject.activeNotifications.Remove(_roomNames[i]);
                    }
                }
            }
            Notifications.TaggedObject.UpdateNotifications();
        }

        /// <summary>
        /// The flexible room lengths for each apartment type
        /// </summary>
        public Dictionary<string, float[]> customVals = new Dictionary<string, float[]>()
        {
            //db,pb,ba,baa,wc,wcc,dbb,pbb,psb
            {"SpecialApt",new float[] {3,3,2.25f,2.1f,1.35f,1.35f, 2.4f, 3.6f, 2.4f } },
            {"Studio", new float[] {3,3,2.25f,2.1f,1.35f,1.35f, 2.4f, 3.6f, 2.4f } },
            {"1b2p", new float[] {3,3,2.25f,2.1f,1.35f,1.35f, 2.4f, 3.6f, 2.4f } },
            {"2b3p", new float[] {3,3, 2.25f, 2.1f, 1.35f, 1.35f, 2.4f, 3.6f, 2.4f } },
            {"2b4p", new float[] {3,3, 2.25f, 2.1f, 1.35f, 1.35f, 2.4f, 3.6f, 2.4f } },
            {"3b5p", new float[] {3,3, 2.25f, 2.1f, 1.35f, 1.35f, 2.4f, 3.6f, 2.4f } },
        };

        /// <summary>
        /// The modules of each apartment type which have a fixed size
        /// </summary>
        public Dictionary<string, float[]> FixedModuleWidths
        {
            get
            {
                return new Dictionary<string, float[]>()
                {
                    {"Studio", new float[] { customVals["Studio"][2] } },
                    {"1b2p", new float[] { customVals["1b2p"][0] } },
                    {"2b3p", new float[] { customVals["2b3p"][0], 2.4f } },
                    {"2b4p", new float[] { customVals["2b4p"][1], 3.6f } },
                    {"3b5p", new float[] { customVals["3b5p"][1], 3.6f,2.4f } }
                };
            }
        }

        /// <summary>
        /// The different line load-spans per apartment type
        /// </summary>
        public Dictionary<string, float[][]> FixedLineLoadsSpans
        {
            get
            {
                return new Dictionary<string, float[][]>()
                {
                    {"Studio", new float[][] { new float[] { customVals["Studio"][2] } , new float[] {} } },
                    {"1b2p", new float[][] { new float[] { customVals["1b2p"][2], 2.4f } , new float[] {customVals["1b2p"][0] } } },
                    {"2b3p", new float[][] { new float[] { customVals["2b3p"][2], 2.4f } , new float[] {customVals["2b3p"][0], 2.4f } } },
                    {"2b4p", new float[][] { new float[] { customVals["2b4p"][2] + 0.75f, 1.5f + customVals["2b4p"][3] } , new float[] {customVals["2b4p"][1], 3.6f } } },
                    {"3b5p", new float[][] { new float[] { customVals["3b5p"][2] + 0.75f, 1.5f + customVals["3b5p"][3], customVals["3b5p"][5] } , new float[] {customVals["3b5p"][1], 3.6f, 2.4f } } },
                };
            }
        }

        // Use this for initialization
        void Start()
        {
            EvaluateStandardRooms();
            EvaluateAmenitiesArea();
        }

        // Update is called once per frame
        void Update()
        {

        }


        /// <summary>
        /// Called by the Unity UI to set the type of the amenities
        /// </summary>
        /// <param name="value">The type of the amenitites</param>
        public void SetBalconies(int value)
        {
            var temp_balc = (int)balconies;
            if (value != temp_balc)
            {
                if (balconies == Balconies.Inset)
                {
                    foreach (var apt in ApartmentTypeRooms)
                    {
                        if (apt.Key != "SpecialApt")
                        {
                            foreach (var index in amenitiesIndices["Interior"])
                            {
                                if (amenitiesIndices["Exterior"].ContainsKey(index.Key))
                                {
                                    if (apt.Value[index.Value])
                                    {
                                        apt.Value[amenitiesIndices["Exterior"][index.Key]] = true;
                                    }
                                }
                                apt.Value[index.Value] = false;
                            }
                        }
                    }
                }
                else
                {
                    foreach (var apt in ApartmentTypeRooms)
                    {
                        if (apt.Key != "SpecialApt")
                        {
                            foreach (var index in amenitiesIndices["Exterior"])
                            {
                                if (amenitiesIndices["Interior"].ContainsKey(index.Key))
                                {
                                    if (apt.Value[index.Value])
                                    {
                                        apt.Value[amenitiesIndices["Interior"][index.Key]] = true;
                                    }
                                }
                                apt.Value[index.Value] = false;
                            }
                        }
                    }
                }
            }
            balconies = (Balconies)value;
        }

        /// <summary>
        /// Sets the minimum area per apartment type
        /// </summary>
        /// <param name="minAreas">The minimum area of each apartment type</param>
        public void SetAptMinimumAreas(Dictionary<string, double> minAreas)
        {
            apartmentTypesMinimumSizes = minAreas;
            if (onAptsAreasChanged != null)
            {
                onAptsAreasChanged.Invoke(this, true);
            }
        }

        /// <summary>
        /// Attempts to set the minimum area of apartment type
        /// </summary>
        /// <param name="key">The apartment type</param>
        /// <param name="value">The area</param>
        /// <returns>Whether it succeeded or not</returns>
        public bool TrySetMinimumArea(string key, double value)
        {
            if (apartmentTypesMinimumSizes.ContainsKey(key))
            {
                apartmentTypesMinimumSizes[key] = value;
                if (value > desiredAreas[key])
                {
                    desiredAreas[key] = value;
                }
                ApartmentTypesMaximumSizes[key] = ApartmentTypesMinimumSizes[key] + ApartmentTypesMinimumSizes[key] * 0.1;
                if (onAptsAreasChanged != null)
                {
                    onAptsAreasChanged.Invoke(this, true);
                }
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Sets the assigned are per apartment type
        /// </summary>
        /// <param name="key">Apartment Type</param>
        /// <param name="value">Assigned Area</param>
        /// <returns>Boolean</returns>
        public bool TrySetDesiredArea(string key, double value)
        {
            if (desiredAreas.ContainsKey(key))
            {
                desiredAreas[key] = value;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Sets the maximum area per apartment type
        /// </summary>
        /// <param name="maxAreas">The maximum area of each apartment type</param>
        public void SetAptMaximumAreas(Dictionary<string, double> maxAreas)
        {
            ApartmentTypesMaximumSizes = maxAreas;
            if (onAptsAreasChanged != null)
            {
                onAptsAreasChanged.Invoke(this, true);
            }
        }

        /// <summary>
        /// Attempts to set the maximum area of apartment type
        /// </summary>
        /// <param name="key">The apartment type</param>
        /// <param name="value">The area</param>
        /// <returns>Whether it succeeded or not</returns>
        public bool TrySetMaximumArea(string key, double value)
        {
            if (ApartmentTypesMaximumSizes.ContainsKey(key))
            {
                ApartmentTypesMaximumSizes[key] = value;

                if (onAptsAreasChanged != null)
                {
                    onAptsAreasChanged.Invoke(this, true);
                }
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Attempts to set the amanity area of apartment type
        /// </summary>
        /// <param name="key">The apartment type</param>
        /// <param name="value">The area</param>
        /// <param name="triggerEvent">Whether there should be an event triggered</param>
        /// <returns>Whether it succeeded or not</returns>
        public bool TrySetAmenitiesArea(string key, double value, bool triggerEvent = true)
        {
            if (amenitiesAreas.ContainsKey(key))
            {
                if (value == 0)
                {
                    amenitiesAreas[key] = (float)value;
                }
                else
                {
                    amenitiesAreas[key] = MinimumAmenitiesArea[key];
                }

                ApartmentTypesMaximumSizes[key] = ApartmentTypesMinimumSizes[key] + ApartmentTypesMinimumSizes[key] * 0.1;
                if (onAptsAreasChanged != null)
                {
                    onAptsAreasChanged.Invoke(this, triggerEvent);
                }
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Sets the corresponding construction feature values based on a given title
        /// </summary>
        /// <param name="title">The construction features preset title</param>
        public void SetConstructionFeaturesToUI(string title)
        {
            if (title == "Custom")
            {
                if (constructionFeaturesElement.constructionTypesDropdown.options[constructionFeaturesElement.constructionTypesDropdown.value].text != "Custom")
                {
                    constructionFeaturesElement.constructionTypesDropdown.options.Add(new Dropdown.OptionData("Custom"));
                    constructionFeaturesElement.constructionTypesDropdown.SetValue(constructionFeaturesElement.constructionTypesDropdown.options.Count - 1);
                    constructionFeaturesElement.constructionTypesDropdown.RefreshShownValue();
                }
            }
            else
            {
                for (int i = 0; i < constructionFeaturesElement.constructionTypesDropdown.options.Count; i++)
                {
                    if (constructionFeaturesElement.constructionTypesDropdown.options[i].text == title)
                    {
                        constructionFeaturesElement.constructionTypesDropdown.SetValue(i);
                        constructionFeaturesElement.constructionTypesDropdown.RefreshShownValue();
                    }
                }
            }

            constructionFeaturesElement.partyWall.SetValue(ConstructionFeatures["PartyWall"].ToString());
            constructionFeaturesElement.exteriorWall.SetValue(ConstructionFeatures["ExteriorWall"].ToString());
            constructionFeaturesElement.corridorWall.SetValue(ConstructionFeatures["CorridorWall"].ToString());
            constructionFeaturesElement.panelLength.SetValue(ConstructionFeatures["PanelLength"].ToString());
            constructionFeaturesElement.lineLoadSpan.SetValue(ConstructionFeatures["LineSpan"].ToString());

            ReportData.TaggedObject.ConstructionFeaturesName = constructionFeaturesElement.constructionTypesDropdown.options[constructionFeaturesElement.constructionTypesDropdown.value].text;
            ReportData.TaggedObject.ConstructionFeatures = Standards.TaggedObject.ConstructionFeaturesReport;

            balconiesDropdown.SetValue((int)balconies);
        }

        /// <summary>
        /// The title of the current construction features preset
        /// </summary>
        public string ConstructionFeaturesTitle
        {
            get
            {
                return constructionFeaturesElement.constructionTypesDropdown.options[constructionFeaturesElement.constructionTypesDropdown.value].text;
            }
        }
    }

    /// <summary>
    /// A UnityEvent class for when the apartment standards change
    /// </summary>
    [System.Serializable]
    public class OnAptAreasChanged : UnityEvent<Standards, bool>
    {

    }
}
