﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for storing data that will be transfered the pdf report javascript method
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ReportData : Tagged<ReportData>
    {
        [JsonProperty]
        public string date { get; set; }
        [JsonProperty]
        public string[] generalWarnings { get; set; }
        [JsonProperty]
        public string location { get; set; }
        [JsonProperty]
        public List<BuildingReportData> buildings { get; set; }
        [JsonProperty]
        public Dictionary<string, string> aptNumbers { get; set; }
        [JsonProperty]
        public Dictionary<string, int> modulesNumbers { get; set; }
        [JsonProperty]
        public Dictionary<string, string> modulesColors { get; set; }
        [JsonProperty]
        public Dictionary<string, int> panelsNumbers { get; set; }
        [JsonProperty]
        public string ConstructionFeaturesName { get; set; }
        [JsonProperty]
        public Dictionary<string, float> ConstructionFeatures { get; set; }
        [JsonProperty]
        public int siteArea { get; set; }
        [JsonProperty]
        public int gea { get; set; }
        [JsonProperty]
        public int gia { get; set; }
        [JsonProperty]
        public int nia { get; set; }
        [JsonProperty]
        public int netgross { get; set; }
        [JsonProperty]
        public float unitDensity { get; set; }
        [JsonProperty]
        public float habitableDensity { get; set; }
        [JsonProperty]
        public string ptalRating { get; set; }
        [JsonProperty]
        public string[] images { get; set; }

        private void Start()
        {

        }
        
        public override string ToString()
        {
            date = DateTime.Now.ToString("dd/MM/yyyy");
            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, StringEscapeHandling = StringEscapeHandling.EscapeHtml, ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
    }

    /// <summary>
    /// A class to handle the data for the pdf report
    /// </summary>
    [System.Serializable]
    public class BuildingReportData
    {
        [JsonProperty]
        public string buildingName;
        [JsonProperty]
        public Dictionary<string, ConstructionSystemData> constructionSystems;
        [JsonProperty]
        public Dictionary<string, string> aptNumbers { get; set; }
        [JsonProperty]
        public int gea;
        [JsonProperty]
        public int gia;
        [JsonProperty]
        public int nia;
        [JsonProperty]
        public int netgross;

        public BuildingReportData()
        {
            constructionSystems = new Dictionary<string, ConstructionSystemData>();
            aptNumbers = new Dictionary<string, string>();
        }
    }

    /// <summary>
    /// A class to store information regarding the evaluation of construction systems
    /// </summary>
    [System.Serializable]
    public class ConstructionSystemData
    {
        public string[] warnings { get; set; }
        public int[] feasibility { get; set; }

        public ConstructionSystemData()
        {
            
        }
    }
}
