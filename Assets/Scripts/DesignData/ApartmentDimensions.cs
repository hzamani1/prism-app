﻿using BrydenWoodUnity.DesignData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.UIElements;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for handling the dimension display of the apartments
    /// </summary>
    public class ApartmentDimensions : MonoBehaviour
    {
        [Header("Scene References:")]
        public Transform dimensionsCanvasParent;
        public InputField offsetInputField;
        public Camera projectionCamera;
        [Header("Assets References:")]
        public GameObject dimensionPrefab;
        public ApartmentAreaRange[] apartAreas;
        [Header("Settings:")]
        public float lowerOffset = 1.5f;
        public float leftOffset = 1.5f;
        public float scale { get { return CalculateScale(); } }
        [HideInInspector]
        public ProceduralApartmentLayout proceduralApartment;

        private Dictionary<string, DynamicDimensions> m_dimensions { get; set; }

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            proceduralApartment = GetComponent<ProceduralApartmentLayout>();

        }

        // Update is called once per frame
        void Update()
        {
            
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Turn dimensions on or off
        /// </summary>
        /// <param name="toggle">Whether the dimensions should be on or off</param>
        public void ToggleDimensions(bool toggle)
        {
            DeleteDimensions();
            if (toggle)
            {
                GenerateDimensions();//StartCoroutine(ResetDimensions());
                dimensionsCanvasParent.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Deletes all apartment dimensions
        /// </summary>
        public void DeleteDimensions()
        {
            if (m_dimensions != null)
            {
                dimensionsCanvasParent.gameObject.SetActive(false);
                m_dimensions = null;
                List<GameObject> objsToDelete = new List<GameObject>();
                foreach (Transform tr in dimensionsCanvasParent)
                {
                    objsToDelete.Add(tr.gameObject);
                }
                for (int i=0; i<objsToDelete.Count; i++)
                {
                    DestroyImmediate(objsToDelete[i]);
                }
            }
        }

        /// <summary>
        /// Generates the dimensions of the rooms
        /// </summary>
        public void GenerateDimensions()
        {
            m_dimensions = new Dictionary<string, DynamicDimensions>();
            { //For the main width dimension

                DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                dimension.gameObject.name = proceduralApartment.gameObject.name + "_Width";
                dimension.scale = scale;

                Vector3 origin = proceduralApartment.transform.position + new Vector3(Standards.TaggedObject.ConstructionFeatures["PartyWall"]/2.0f,0,0);
                Vector3 end = origin + new Vector3(proceduralApartment.width, 0, 0);

                origin = new Vector3(origin.x, 0, lowerOffset * 2);
                end = new Vector3(end.x, 0, lowerOffset * 2);

                dimension.startAnchor = projectionCamera.WorldToScreenPoint(origin);
                dimension.endAnchor = projectionCamera.WorldToScreenPoint(end);
                dimension.horizontal = true;
                dimension.enableDrag = true;
                dimension.upSide_outSide = false;
                dimension.readOnly = proceduralApartment.readOnly;
                dimension.SetAsCancelable();
                dimension.SetLine();
                dimension.valueChanged += OnDimensionChanged;
                m_dimensions.Add("width", dimension);
            }

            { //For the main height dimension

                DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                dimension.gameObject.name = proceduralApartment.gameObject.name + "_Depth";
                dimension.scale = scale;

                Vector3 origin = proceduralApartment.transform.position + new Vector3(0,0, Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                Vector3 end = origin + new Vector3(0, 0, proceduralApartment.height);

                origin = new Vector3(leftOffset * 2, 0, origin.z);
                end = new Vector3(leftOffset * 2, 0, end.z);

                dimension.startAnchor = projectionCamera.WorldToScreenPoint(origin);
                dimension.endAnchor = projectionCamera.WorldToScreenPoint(end);
                dimension.horizontal = true;
                dimension.enableDrag = true;
                dimension.upSide_outSide = true;
                dimension.readOnly = true;
                dimension.SetLine();
                m_dimensions.Add("height", dimension);
            }

            for (int i = 0; i < proceduralApartment.rooms.Count; i++)
            {
                if (proceduralApartment.rooms[i].lengths[0] != 0 && proceduralApartment.rooms[i].showDimension)
                {
                    DynamicDimensions dimension = Instantiate(dimensionPrefab, dimensionsCanvasParent).GetComponent<DynamicDimensions>();
                    dimension.gameObject.name = proceduralApartment.rooms[i].gameObject.name;
                    dimension.scale = scale;

                    Vector3 origin = proceduralApartment.rooms[i].transform.TransformPoint(proceduralApartment.rooms[i].dimensionStart);
                    Vector3 end = proceduralApartment.rooms[i].transform.TransformPoint(proceduralApartment.rooms[i].dimensionEnd);

                    switch (proceduralApartment.rooms[i].dimensionSide)
                    {
                        case 0:
                            origin = new Vector3(leftOffset, 0, origin.z);
                            end = new Vector3(leftOffset, 0, end.z);
                            dimension.upSide_outSide = true;
                            break;
                        case 1:
                            origin = new Vector3(origin.x, 0, lowerOffset);
                            end = new Vector3(end.x, 0, lowerOffset);
                            dimension.upSide_outSide = false;
                            break;
                        case 2:
                            origin = new Vector3(proceduralApartment.width - leftOffset, 0, origin.z);
                            end = new Vector3(proceduralApartment.width - leftOffset, 0, end.z);
                            dimension.upSide_outSide = false;
                            break;
                        case 3:
                            origin = new Vector3(origin.x, 0, proceduralApartment.height - 2*lowerOffset);
                            end = new Vector3(end.x, 0, proceduralApartment.height - 2*lowerOffset);
                            dimension.upSide_outSide = true;
                            break;
                    }

                    dimension.startAnchor = projectionCamera.WorldToScreenPoint(origin);
                    dimension.endAnchor = projectionCamera.WorldToScreenPoint(end);
                    dimension.horizontal = true;
                    dimension.enableDrag = true;
                    if (!proceduralApartment.readOnly)
                    {
                        if (dimension.gameObject.name == "DB" ||
                            dimension.gameObject.name == "PB" ||
                            dimension.gameObject.name == "BA" ||
                            dimension.gameObject.name == "BAA" ||
                            dimension.gameObject.name == "WC" ||
                            dimension.gameObject.name == "WCC" ||
                            dimension.gameObject.name == "DBB" ||
                            dimension.gameObject.name == "PBB" ||
                            dimension.gameObject.name == "PSB")
                        {
                            dimension.readOnly = false;
                        }
                        else
                        {
                            dimension.readOnly = true;
                        }
                    }
                    else
                    {
                        dimension.readOnly = true;
                    }
                    dimension.SetLine();
                    dimension.valueChanged += OnDimensionChanged;
                    m_dimensions.Add(proceduralApartment.rooms[i].gameObject.name, dimension);
                }
            }
        }
        #endregion

        #region Private Methods

        private IEnumerator UpdateDims()
        {
            yield return StartCoroutine(proceduralApartment.ResetLayout());
            proceduralApartment.Initialize();
            proceduralApartment.ShowOutline(true);
            proceduralApartment.ToggleManufacturingSystem();
            UpdateLines();
        }

        private IEnumerator UpdateDims2()
        {
            yield return StartCoroutine(proceduralApartment.ResetLayout());
            proceduralApartment.ReInitialize();
            proceduralApartment.ToggleManufacturingSystem();
            UpdateLines();
        }

        private void UpdateLines()
        {
            foreach (var item in m_dimensions)
            {
                var dimension = item.Value;
                if (item.Key == "width")
                { //For the main width dimension

                    Vector3 origin = proceduralApartment.transform.position;
                    Vector3 end = proceduralApartment.transform.position + new Vector3(proceduralApartment.width, 0, 0);

                    origin = new Vector3(origin.x, 0, lowerOffset * 2);
                    end = new Vector3(end.x, 0, lowerOffset * 2);

                    dimension.startAnchor = projectionCamera.WorldToScreenPoint(origin);
                    dimension.endAnchor = projectionCamera.WorldToScreenPoint(end);
                    dimension.SetLine();
                }
                else if (item.Key == "height")
                { //For the main width dimension

                    Vector3 origin = proceduralApartment.transform.position;
                    Vector3 end = proceduralApartment.transform.position + new Vector3(0, 0, proceduralApartment.height);

                    origin = new Vector3(leftOffset * 2, 0, origin.z);
                    end = new Vector3(leftOffset * 2, 0, end.z);

                    dimension.startAnchor = projectionCamera.WorldToScreenPoint(origin);
                    dimension.endAnchor = projectionCamera.WorldToScreenPoint(end);
                    dimension.SetLine();
                }
                else
                {
                    for (int i = 0; i < proceduralApartment.rooms.Count; i++)
                    {
                        if (proceduralApartment.rooms[i].lengths[0] != 0 && item.Key == proceduralApartment.rooms[i].gameObject.name)
                        {

                            Vector3 origin = proceduralApartment.rooms[i].transform.TransformPoint(proceduralApartment.rooms[i].dimensionStart);
                            Vector3 end = proceduralApartment.rooms[i].transform.TransformPoint(proceduralApartment.rooms[i].dimensionEnd);

                            switch (proceduralApartment.rooms[i].dimensionSide)
                            {
                                case 0:
                                    origin = new Vector3(leftOffset, 0, origin.z);
                                    end = new Vector3(leftOffset, 0, end.z);
                                    dimension.upSide_outSide = true;
                                    break;
                                case 1:
                                    origin = new Vector3(origin.x, 0, lowerOffset);
                                    end = new Vector3(end.x, 0, lowerOffset);
                                    dimension.upSide_outSide = false;
                                    break;
                                case 2:
                                    origin = new Vector3(proceduralApartment.width - leftOffset, 0, origin.z);
                                    end = new Vector3(proceduralApartment.width - leftOffset, 0, end.z);
                                    dimension.upSide_outSide = false;
                                    break;
                                case 3:
                                    origin = new Vector3(origin.x, 0, proceduralApartment.height - lowerOffset*2);
                                    end = new Vector3(end.x, 0, proceduralApartment.height - lowerOffset*2);
                                    dimension.upSide_outSide = true;
                                    break;
                            }

                            dimension.startAnchor = projectionCamera.WorldToScreenPoint(origin);
                            dimension.endAnchor = projectionCamera.WorldToScreenPoint(end);
                            dimension.SetLine();
                        }
                    }
                }
            }
        }

        private float CalculateScale()
        {
            float scale = 1;

            var proj1 = projectionCamera.WorldToScreenPoint(Vector3.zero);
            var proj2 = projectionCamera.WorldToScreenPoint(new Vector3(1, 0, 0));
            scale = Vector3.Distance(proj1, proj2);

            return scale;
        }

        private void OnDimensionChanged(DynamicDimensions sender)
        {
            foreach (var item in m_dimensions)
            {
                if (item.Value == sender)
                {
                    if (item.Key == "width")
                    {
                        proceduralApartment.width = sender.Value;
                        //for (int i = 0; i < apartAreas.Length; i++)
                        //{
                        //    if (apartAreas[i].type == proceduralApartment.apartmentType)
                        //    {
                        //        apartAreas[i].areaRange.text = (proceduralApartment.width * proceduralApartment.height).ToString();
                        //    }
                        //}
                        if (Standards.TaggedObject.balconies == Balconies.External)
                        {
                            var amenitiesArea = proceduralApartment.AmenitySpace;
                            Standards.TaggedObject.TrySetAmenitiesArea(proceduralApartment.apartmentType, amenitiesArea, false);
                            //Standards.TaggedObject.amenitiesAreas[proceduralApartment.apartmentType] = (float)amenitiesArea;
                            Standards.TaggedObject.TrySetDesiredArea(proceduralApartment.apartmentType, (proceduralApartment.width * proceduralApartment.height));
                            //Standards.TaggedObject.apartmentTypesMinimumSizes[proceduralApartment.apartmentType] = (proceduralApartment.width * proceduralApartment.height);
                        }
                        else
                        {
                            var area = proceduralApartment.width * proceduralApartment.height;
                            var amenitiesArea = proceduralApartment.AmenitySpace;
                            //Standards.TaggedObject.amenitiesAreas[proceduralApartment.apartmentType] = (float)amenitiesArea;
                            Standards.TaggedObject.TrySetAmenitiesArea(proceduralApartment.apartmentType, amenitiesArea, false);
                            //Standards.TaggedObject.apartmentTypesMinimumSizes[proceduralApartment.apartmentType] = (area - amenitiesArea);
                            Standards.TaggedObject.TrySetDesiredArea(proceduralApartment.apartmentType, (area - amenitiesArea));
                            Debug.Log("Test");
                        }
                    }
                    else if (item.Key == "height")
                    {
                        proceduralApartment.height = sender.Value;
                        offsetInputField.text = proceduralApartment.height.ToString();
                    }
                    else if (item.Key == "DB")
                    {
                        Standards.TaggedObject.customVals[proceduralApartment.apartmentType][0] = sender.Value;
                    }
                    else if (item.Key == "PB")
                    {
                        Standards.TaggedObject.customVals[proceduralApartment.apartmentType][1] = sender.Value;
                    }
                    else if (item.Key == "BA")
                    {
                        Standards.TaggedObject.customVals[proceduralApartment.apartmentType][2] = sender.Value;
                    }
                    else if (item.Key == "BAA")
                    {
                        Standards.TaggedObject.customVals[proceduralApartment.apartmentType][3] = sender.Value;
                    }
                    else if (item.Key == "WC")
                    {
                        Standards.TaggedObject.customVals[proceduralApartment.apartmentType][4] = sender.Value;
                    }
                    else if (item.Key == "WCC")
                    {
                        Standards.TaggedObject.customVals[proceduralApartment.apartmentType][5] = sender.Value;
                    }
                    else if (item.Key == "DBB")
                    {
                        Standards.TaggedObject.customVals[proceduralApartment.apartmentType][6] = sender.Value;
                    }
                    else if (item.Key == "PBB")
                    {
                        Standards.TaggedObject.customVals[proceduralApartment.apartmentType][7] = sender.Value;
                    }
                    else if (item.Key == "PSB")
                    {
                        Standards.TaggedObject.customVals[proceduralApartment.apartmentType][8] = sender.Value;
                    }
                    StartCoroutine(UpdateDims());
                    break;
                }
            }
        }
        #endregion



    }
}
