﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// Preview mode for the massing
    /// </summary>
    public enum PreviewMode
    {
        /// <summary>
        /// Shows the buildings in full height
        /// </summary>
        Buildings,
        /// <summary>
        /// Shows one instance of each floor layout
        /// </summary>
        Floors,
        /// <summary>
        /// Shows an instance of each apartment type
        /// </summary>
        Apartments,
    }

    /// <summary>
    /// The different manufacturing systems
    /// </summary>
    public enum ManufacturingSystem
    {
        /// <summary>
        /// No manufacturing system is showing
        /// </summary>
        Off = 0,
        /// <summary>
        /// Shows the manufacturing system of volumetric modules
        /// </summary>
        VolumetricModules = 1,
        /// <summary>
        /// Shows the facade panels
        /// </summary>
        Panels = 2,
        /// <summary>
        /// Shows the line-loads
        /// </summary>
        LineLoads = 3,
    }

    /// <summary>
    /// The allignment of the core regarding the corridor
    /// </summary>
    public enum CoreAllignment
    {
        /// <summary>
        /// The core is on the same axis as the central corridor
        /// </summary>
        Centre = 0,
        /// <summary>
        /// The core is on the left of the central corridor
        /// </summary>
        Left = 1,
        /// <summary>
        /// The cors is on the right of the central corridor
        /// </summary>
        Right = 2
    }

    /// <summary>
    /// The type of linear building based on its aspect
    /// </summary>
    public enum LinearTypology
    {
        /// <summary>
        /// Single Aspect Linear Building (Corridor in the middle and apartments on eighter side)
        /// </summary>
        Single = 0,
        /// <summary>
        /// Deck Access Linear Building (Apartments on one side of the corridor)
        /// </summary>
        DeckAccess = 1,
    }

    /// <summary>
    /// The type of the building
    /// </summary>
    public enum BuildingType
    {
        /// <summary>
        /// Linear Building
        /// </summary>
        Linear = 0,
        /// <summary>
        /// Tower Building
        /// </summary>
        Tower = 1
    }

    /// <summary>
    /// The type of amenities in the project
    /// </summary>
    public enum Balconies
    {
        /// <summary>
        /// External Balconies
        /// </summary>
        External = 0,
        /// <summary>
        /// Inset balconies
        /// </summary>
        Inset = 1
    }


}
