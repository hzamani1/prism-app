﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for Procedural Rooms
    /// </summary>
    public class ProceduralRoom : MonoBehaviour, ICancelable
    {
        #region Public Properties
        public GameObject polygonPrefab;
        public GameObject polygonVertexPrefab;
        public GameObject extrusionPrefab;
        public GameObject lineLoadPrefab;
        public GameObject amenitiesButtonPrefab;
        public Material roomMaterial;
        [HideInInspector]
        public ProceduralApartmentLayout aptLayout;
        public List<float> lengths;
        [HideInInspector]
        public List<Vector3> vertices;
        [HideInInspector]
        public Color color;


        public Polygon polygon { get; set; }
        public bool hasBalcony { get; set; }
        public float totalHeight { get; set; }
        public int roomIndex { get; set; }
        public bool isBalcony { get; set; }
        public int dimensionStartIndex { get; set; }
        public int dimensionEndIndex { get; set; }
        public int dimensionSide { get; set; }
        public float[] originalLengths { get; set; }
        public Vector3 dimensionStart
        {
            get
            {
                return vertices[dimensionStartIndex];
            }
        }
        public Vector3 dimensionEnd
        {
            get
            {
                return vertices[dimensionEndIndex];
            }
        }
        public bool showDimension;
        public bool issue { get; private set; }
        #endregion

        #region Private Properties
        private Vector3 currentPoint { get; set; }
        private Text roomName { get; set; }
        private List<GameObject> lineLoads { get; set; }
        private List<LineLoad> issues { get; set; }
        private GameObject amenitiesUIObject { get; set; }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            CheckMinDm();
            if (amenitiesUIObject != null)
            {
                Destroy(amenitiesUIObject);
            }
            if (roomName != null)
            {
                DestroyImmediate(roomName.gameObject);
            }
            //ShowRoomName(false);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="lengths">The lengths of the room's sides as a list</param>
        public void Initialize(List<float> lengths)
        {
            this.lengths = lengths;
            GetVectors();
            GenerateGeometry();
            GenerateLineLoads();
            ToggleLineLoads(false);
            CheckMinDm();
        }

        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="lengths">The lengths of the room's sides as a list</param>
        /// <param name="type">The type of the room</param>
        /// <param name="totalHeight">The total height of the room</param>
        /// <param name="index">The index of the room</param>
        /// <param name="showDimension">Whether the room should display its dimensions</param>
        public void Initialize(List<float> lengths, string type, float totalHeight, int index, bool showDimension)
        {
            this.totalHeight = totalHeight;
            this.showDimension = showDimension;
            gameObject.AddComponent<RoomUnity>().Initialize(index, type);
            this.lengths = lengths;
            roomIndex = index;
            isBalcony = false;
            foreach (var item in Standards.TaggedObject.amenitiesIndices)
            {
                foreach (var balc in item.Value)
                {
                    if (index == balc.Value)
                    {
                        isBalcony = true;
                    }
                }
            }

            if (isBalcony)
            {
                this.totalHeight = 1.0f;
            }

            originalLengths = new float[lengths.Count];

            for (int i = 0; i < originalLengths.Length; i++)
            {
                originalLengths[i] = lengths[i];
            }

            GetVectors();
            GenerateGeometry();
            ShowRoomName(true);
            dimensionStartIndex = Standards.TaggedObject.RoomsDimensionIndices[gameObject.name][0];
            dimensionEndIndex = Standards.TaggedObject.RoomsDimensionIndices[gameObject.name][1];
            dimensionSide = Standards.TaggedObject.RoomsDimensionIndices[gameObject.name][2];
            GenerateLineLoads();
            ToggleLineLoads(false);
            CheckMinDm();
            if (showDimension)
            {
                CreateAmenitiesButton();
            }
        }

        /// <summary>
        /// Updates the room geometry
        /// </summary>
        /// <param name="lengths">The lengths of the room's sides as a list</param>
        public void UpdateRoom(List<float> lengths)
        {
            this.lengths = lengths;
            GetVectors();
            UpdateGeometry();
            GenerateLineLoads();
            ToggleLineLoads(aptLayout.manufacturingSystem == ManufacturingSystem.Panels);
            CheckMinDm();
        }

        /// <summary>
        /// Returns the positions of the room vertex at a given index
        /// </summary>
        /// <param name="index">The index of the vertex</param>
        /// <returns></returns>
        public Vector3 GetVertex(int index)
        {
            return transform.TransformPoint(vertices[index]);
        }

        /// <summary>
        /// Toggle the display of the name of the room
        /// </summary>
        /// <param name="show">Whether the name should be displayed or not</param>
        public void ShowRoomName(bool show)
        {
            var cam = GameObject.Find("TopViewCamera");
            if (cam != null)
            {
                var centre = polygon.Centre;
                if (show && centre != Vector3.zero && lengths[0]!=0)
                {
                    if (roomName == null)
                    {
                        roomName = Instantiate(Resources.Load("GUI/RoomName") as GameObject).GetComponent<Text>();
                        roomName.transform.position = cam.GetComponent<Camera>().WorldToScreenPoint(transform.TransformPoint(polygon.Centre));
                        roomName.transform.SetParent(GameObject.Find("RoomNames").transform);
                    }

                    if (gameObject.name == "BA")
                    {
                        if (aptLayout.apartmentType == "2b4p" || aptLayout.apartmentType == "3b5p")
                        {
                            roomName.text = "Ensuite" + "\r\n" + Math.Round(polygon.Area) + "m\xB2";
                        }
                        else
                        {
                            roomName.text = Standards.TaggedObject.RoomNames[gameObject.name] + "\r\n" + Math.Round(polygon.Area) + "m\xB2";
                        }
                    }
                    else
                        roomName.text = Standards.TaggedObject.RoomNames[gameObject.name] + "\r\n" + Math.Round(polygon.Area) + "m\xB2";
                }
                else
                {
                    if (roomName != null)
                    {
                        DestroyImmediate(roomName.gameObject);
                    }
                }
            }
        }

        /// <summary>
        /// Toggles the line loads On or Off
        /// </summary>
        /// <param name="show">On or Off</param>
        public void ToggleLineLoads(bool show)
        {
            if (lineLoads == null && show)
            {
                GenerateLineLoads();
            }

            if (lineLoads != null)
            {
                for (int i = 0; i < lineLoads.Count; i++)
                {
                    if (lineLoads[i] != null)
                    {
                        lineLoads[i].GetComponent<MeshRenderer>().enabled = show;
                    }
                }
            }
        }

        /// <summary>
        /// Returns the game objects of the line loads
        /// </summary>
        /// <returns>List of GameObjects</returns>
        public List<GameObject> GetLineLoads()
        {
            List<GameObject> loads = new List<GameObject>();
            if (lineLoads != null)
            {
                for (int i = 0; i < lineLoads.Count; i++)
                {
                    if (!lineLoads[i].GetComponent<LineLoad>().issue)
                    {
                        loads.Add(lineLoads[i]);
                    }
                }
            }
            return loads;
        }

        /// <summary>
        /// Returns the game objects of the line loads for a given height
        /// </summary>
        /// <param name="height">The height of the gameobjects</param>
        /// <returns>List of GameObjects</returns>
        public List<GameObject> GetLineLoads(float height)
        {
            List<GameObject> loads = new List<GameObject>();
            if (lengths[0] != 0)
            {
                if (Standards.TaggedObject.LineLoadsPerType[aptLayout.apartmentType].Contains(gameObject.name))
                {
                    var points = Standards.TaggedObject.lineLoadStart[gameObject.name];

                    for (int i = 0; i < points.Length; i++)
                    {
                        var index = points[i][0];
                        var next = (index + 1) % polygon.Count;

                        float length = aptLayout.outterHeight;
                        GameObject obj = Instantiate(lineLoadPrefab, transform);
                        if (points[i][1] == 0)
                        {
                            obj.transform.position = new Vector3(transform.TransformPoint(vertices[index]).x, height / 2.0f, length * 0.25f);
                            obj.transform.localScale = new Vector3(0.03f, height * 0.98f, length * 0.5f);
                            obj.layer = 17;
                        }
                        else
                        {
                            obj.layer = 18;
                            obj.transform.position = new Vector3(transform.TransformPoint(vertices[index]).x, height / 2.0f, length * 0.75f);
                            obj.transform.localScale = new Vector3(0.03f, height * 0.98f, length * 0.5f);
                        }
                        loads.Add(obj);
                        aptLayout.AddLineLoad(obj, points[i][1]);
                    }
                }
            }
            return loads;
        }

        /// <summary>
        /// Create the button for adding and removing amenity space
        /// </summary>
        public void CreateAmenitiesButton()
        {
            var cam = GameObject.Find("TopViewCamera");
            if (cam != null && lengths[0] != 0)
            {
                if (Standards.TaggedObject.balconies == Balconies.Inset)
                {
                    if (name == "LRR" || name == "LR")
                    {
                        amenitiesUIObject = Instantiate(amenitiesButtonPrefab, GameObject.Find("AmenitiesButtons").transform);
                        Vector3 pos = transform.TransformPoint((dimensionStart + (dimensionEnd - dimensionStart) / 2) + new Vector3(0, 0, -1.0f));
                        amenitiesUIObject.transform.position = cam.GetComponent<Camera>().WorldToScreenPoint(pos);
                        amenitiesUIObject.GetComponent<Button>().onClick.AddListener(CreateBalcony);
                        int indexOfBalcony = -1;
                        if (Standards.TaggedObject.amenitiesIndices["Interior"].TryGetValue(GetComponent<RoomUnity>().id, out indexOfBalcony))
                        {
                            amenitiesUIObject.transform.GetChild(0).GetComponent<Text>().text = (aptLayout.roomInclusion[indexOfBalcony]) ? "-" : "+";
                        }
                    }
                }
                else
                {
                    if (name == "LRR" || name == "LR")
                    {
                        //if (name == "DB" ||
                        //    name == "DBB" ||
                        //    name == "PB" ||
                        //    name == "PBB" ||
                        //    name == "LR" ||
                        //    name == "LRR" ||
                        //    name == "PSB")
                        //{

                        amenitiesUIObject = Instantiate(amenitiesButtonPrefab, GameObject.Find("AmenitiesButtons").transform);
                        Vector3 pos = transform.TransformPoint((dimensionStart + (dimensionEnd - dimensionStart) / 2) + new Vector3(0, 0, -1.0f));
                        amenitiesUIObject.transform.position = cam.GetComponent<Camera>().WorldToScreenPoint(pos);
                        amenitiesUIObject.GetComponent<Button>().onClick.AddListener(CreateBalcony);
                        int indexOfBalcony = -1;
                        if (Standards.TaggedObject.amenitiesIndices["Exterior"].TryGetValue(GetComponent<RoomUnity>().id, out indexOfBalcony))
                        {
                            amenitiesUIObject.transform.GetChild(0).GetComponent<Text>().text = (aptLayout.roomInclusion[indexOfBalcony]) ? "-" : "+";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Toggles the amenity space of thius room
        /// </summary>
        public void CreateBalcony()
        {
            if (Standards.TaggedObject.balconies == Balconies.Inset)
            {
                RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { String.Empty, String.Empty });
            }
            else
            {
                float length = 0;
                if (name == "LR" || name == "LRR")
                {
                    length = lengths[2];
                }
                aptLayout.UpdateAmenities(GetComponent<RoomUnity>().id, length);
            }
        }

        /// <summary>
        /// Checks if there are any issues with the panels
        /// </summary>
        /// <returns></returns>
        public bool CheckIfPanelIssues()
        {
            int counter = 0;
            for (int i = 0; i < lineLoads.Count; i++)
            {
                if (lineLoads[i].GetComponent<LineLoad>().issue)
                {
                    counter++;
                }
            }
            return counter != 0;
        }

        /// <summary>
        /// Resets the value of the amenity button
        /// </summary>
        /// <param name="prevValue"></param>
        public void ResetValue(string prevValue)
        {

        }

        /// <summary>
        /// Submits the new value of the amenity button
        /// </summary>
        /// <param name="currentValue"></param>
        public void SubmitValue(string currentValue)
        {
            float length = 0;
            if (name == "LR" || name == "LRR")
            {
                length = lengths[2];
            }
            aptLayout.UpdateAmenities(GetComponent<RoomUnity>().id, length);
        }
        #endregion

        #region Private Methods
        private void CheckMinDm()
        {
            //if (lengths[0] != 0)
            //{
            //    bool widthIssue = false;
            //    float minWidth = 0.0f;
            //    if ((gameObject.name == "LRR" || gameObject.name == "LR"))
            //    {
            //        if ((lengths[0] <= aptLayout.wallWidth && lengths[0] != 0))
            //        {
            //            SendMessageUpwards("IssuedFlagged", true);
            //        }
            //        else if (lengths[0] != 0 && lengths[0] > aptLayout.wallWidth)
            //        {
            //            SendMessageUpwards("IssuedFlagged", false);
            //        }
            //        if (Standards.TaggedObject.MinimumRoomWidths.TryGetValue(gameObject.name, out minWidth))
            //        {
            //            widthIssue = minWidth > Mathf.Abs(lengths[2]);
            //        }
            //    }
            //    else if (gameObject.name == "DB")
            //    {
            //        if (Standards.TaggedObject.MinimumRoomWidths.TryGetValue(gameObject.name, out minWidth))
            //        {
            //            widthIssue = minWidth > Mathf.Abs(lengths[4]);
            //        }
            //    }
            //    else
            //    {
            //        if (Standards.TaggedObject.MinimumRoomWidths.TryGetValue(gameObject.name, out minWidth))
            //        {
            //            widthIssue = minWidth > Mathf.Abs(lengths[0]);
            //        }
            //    }

            //    string widthNotification = string.Format("The {0} in the standard {1} is too narrow!", Standards.TaggedObject.RoomNames[gameObject.name], aptLayout.apartmentType);

            //    if (widthIssue)
            //    {
            //        Notifications.TaggedObject.AddRoomNotification(widthNotification);
            //        Notifications.TaggedObject.UpdateNotifications();
            //    }
            //    else
            //    {
            //        Notifications.TaggedObject.RemoveRoomNotification(widthNotification);
            //        Notifications.TaggedObject.UpdateNotifications();
            //    }
            //}
        }

        private void GetVectors()
        {
            vertices = new List<Vector3>();
            currentPoint = Vector3.zero;
            vertices.Add(currentPoint);
            for (int i = 0; i < lengths.Count; i++)
            {
                if (i % 2 == 0)
                {
                    currentPoint += new Vector3(lengths[i], 0, 0);
                }
                else
                {
                    currentPoint += new Vector3(0, 0, lengths[i]);
                }
                vertices.Add(new Vector3(currentPoint.x, currentPoint.y, currentPoint.z));
            }
        }

        private void GenerateGeometry()
        {
            List<PolygonVertex> verts = new List<PolygonVertex>();

            List<Vector3> temp_verts = new List<Vector3>();
            List<Vector3> offsets = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                bool allowed = true;
                for (int j = 0; j < temp_verts.Count; j++)
                {
                    if (Vector3.Distance(vertices[i], temp_verts[j]) < 0.05f)
                    {
                        allowed = false;
                    }
                }
                if (allowed)
                {
                    var o = new GameObject(name + ":" + temp_verts.Count);
                    temp_verts.Add(vertices[i]);
                    o.transform.SetParent(transform);
                    o.transform.localPosition = vertices[i];
                }
            }

            bool isClockWise = Polygon.IsClockWise(temp_verts);

            for (int i = 0; i < temp_verts.Count; i++)
            {
                int next = (i + 1) % temp_verts.Count;
                int prev = i - 1;
                if (i - 1 == -1)
                {
                    prev = temp_verts.Count - 1;
                }
                Vector3 v1 = temp_verts[prev] - temp_verts[i];
                Vector3 v2 = temp_verts[next] - temp_verts[i];
                v1.Normalize();
                v2.Normalize();
                var cross = Vector3.Cross(v1, v2);
                float angle = Vector3.Angle(v1, v2);
                float vectorLength;
                if (Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f) != 0)
                {
                    vectorLength = (aptLayout.wallWidth / 2.0f) / Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f);
                }
                else
                {
                    vectorLength = (aptLayout.wallWidth / 2.0f);
                }
                var addition = (v1 + v2);
                addition.Normalize();
                addition *= vectorLength;
                if (Vector3.Cross(v1, v2).y < 0)
                {
                    addition *= -1;
                }
                if (isClockWise)
                {
                    addition *= -1;
                }

                if (addition.magnitude == 0)
                {
                    addition = Vector3.Cross(v1, Vector3.up);
                    addition.Normalize();
                    addition *= -vectorLength;
                }

                offsets.Add(temp_verts[i] + addition);
            }

            for (int i = 0; i < offsets.Count; i++)
            {
                var obj = Instantiate(polygonVertexPrefab, transform);
                verts.Add(obj.GetComponent<PolygonVertex>());
                verts[i].Initialize(i);
                verts[i].UpdatePosition(offsets[i]);
            }
            polygon = Instantiate(polygonPrefab).GetComponent<Polygon>();
            polygon.GetComponent<Renderer>().material = roomMaterial;//new Material(polygon.GetComponent<Renderer>().material);
            //polygon.GetComponent<Renderer>().material.color = color;
            polygon.Initialize(verts, true, true, false);
            polygon.transform.SetParent(transform);
            polygon.transform.localPosition = Vector3.zero;

            GameObject extr = Instantiate(extrusionPrefab, polygon.transform);
            extr.GetComponent<Extrusion>().capped = false;
            extr.GetComponent<Extrusion>().convex = false;
            extr.GetComponent<Extrusion>().totalHeight = totalHeight;
            extr.GetComponent<Extrusion>().Initialize(polygon);

            polygon.SetVertexColors(color);
            GetComponent<RoomUnity>().SetPolygon(polygon);
            if (Standards.TaggedObject.TotalAmenitiesArea.ContainsKey(aptLayout.apartmentType))
            {
                if (Standards.TaggedObject.TotalAmenitiesArea[aptLayout.apartmentType].ContainsKey(gameObject.name))
                {
                    Standards.TaggedObject.TotalAmenitiesArea[aptLayout.apartmentType][gameObject.name] = (float)Math.Round(polygon.Area);
                }
            }
        }

        private void GenerateLineLoads()
        {
            if (lengths[0] != 0)
            {
                if (lineLoads != null)
                {
                    foreach (var item in lineLoads)
                    {
                        DestroyImmediate(item);
                    }
                }

                lineLoads = new List<GameObject>();
                issues = new List<LineLoad>();
                if (Standards.TaggedObject.LineLoadsPerType[aptLayout.apartmentType].Contains(gameObject.name))
                {
                    var points = Standards.TaggedObject.lineLoadStart[gameObject.name];

                    for (int i = 0; i < points.Length; i++)
                    {
                        var index = points[i][0];
                        var next = (index + 1) % polygon.Count;

                        float length = aptLayout.outterHeight;//Vector3.Distance(vertices[index], vertices[next]) + 0.02f;
                        GameObject obj = Instantiate(lineLoadPrefab, transform);
                        obj.GetComponent<LineLoad>().room = this;
                        if (points[i][1] == 0)
                        {
                            Vector3 position = new Vector3(transform.TransformPoint(vertices[index]).x, -1.0f, length * 0.25f);
                            obj.layer = 17;
                            obj.transform.position = position;
                            obj.transform.localScale = new Vector3(0.03f, 0.03f, length * 0.5f);
                        }
                        else
                        {
                            Vector3 position = new Vector3(transform.TransformPoint(vertices[index]).x, -1.0f, length * 0.75f);
                            obj.layer = 18;
                            obj.transform.position = position;
                            obj.transform.localScale = new Vector3(0.03f, 0.03f, length * 0.5f);
                        }
                        lineLoads.Add(obj);
                        aptLayout.AddLineLoad(obj, points[i][1]);
                    }
                }

                if (Standards.TaggedObject.ValidLineLoads.ContainsKey(aptLayout.apartmentType))
                {
                    if (!Standards.TaggedObject.ValidLineLoads[aptLayout.apartmentType].Contains(name))
                    {
                        Standards.TaggedObject.ValidLineLoads[aptLayout.apartmentType].Add(name);
                    }
                }
                else
                {
                    Standards.TaggedObject.ValidLineLoads.Add(aptLayout.apartmentType, new List<string>() { name });
                }
            }
        }

        private void UpdateGeometry()
        {
            List<Vector3> temp_verts = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                bool allowed = true;
                for (int j = 0; j < temp_verts.Count; j++)
                {
                    if (Vector3.Distance(vertices[i], temp_verts[j]) < 0.05f)
                    {
                        allowed = false;
                    }
                }
                if (allowed)
                {
                    temp_verts.Add(vertices[i]);
                }
            }

            bool isClockWise = Polygon.IsClockWise(temp_verts);

            for (int i = 0; i < temp_verts.Count; i++)
            {
                int next = (i + 1) % temp_verts.Count;
                int prev = i - 1;
                if (i - 1 == -1)
                {
                    prev = temp_verts.Count - 1;
                }
                Vector3 v1 = temp_verts[prev] - temp_verts[i];
                Vector3 v2 = temp_verts[next] - temp_verts[i];
                v1.Normalize();
                v2.Normalize();
                v1 *= (aptLayout.wallWidth / 2.0f);
                v2 *= (aptLayout.wallWidth / 2.0f);
                var cross = Vector3.Cross(v1, v2);
                var addition = (v1 + v2);
                if (Vector3.Angle(v1, v2) > 170)
                {
                    var c = Vector3.Cross(v2, Vector3.up);
                    temp_verts[i] = (temp_verts[i] + c.normalized * (aptLayout.wallWidth / 2.0f));
                }
                else
                {
                    if (cross.y < 0 == !isClockWise)
                    {
                        addition *= -1;
                    }
                    temp_verts[i] = (temp_verts[i] + addition);
                }
            }

            for (int i = 0; i < polygon.Count; i++)
            {
                polygon.transform.SetParent(null);
                polygon.transform.position = Vector3.zero;
                polygon[i].UpdatePosition(temp_verts[i]);
                polygon.transform.SetParent(transform);
                polygon.transform.localPosition = Vector3.zero;
            }

            polygon.SetVertexColors(color);
        }
        #endregion
    }
}
