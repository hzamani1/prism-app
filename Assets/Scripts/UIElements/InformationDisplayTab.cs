﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for controlling the infromation display tab
    /// </summary>
    public class InformationDisplayTab : MonoBehaviour
    {
        #region Public Fields and Properties
        public List<Button> buttons;
        public List<Transform> tabs;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            for (int i = 0; i < tabs.Count; i++)
            {
                if (i == 0)
                {
                    tabs[i].SetAsLastSibling();
                    buttons[i].targetGraphic.color = buttons[i].colors.normalColor;
                }
                else
                {
                    buttons[i].targetGraphic.color = buttons[i].colors.pressedColor;
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets the active tab based on a given index
        /// </summary>
        /// <param name="value">The index of the tab to be selected</param>
        public void SetTabByIndex(int value)
        {
            for (int i = 0; i < tabs.Count; i++)
            {
                if (i == value)
                {
                    tabs[i].SetAsLastSibling();
                    buttons[i].targetGraphic.color = buttons[i].colors.normalColor;
                }
                else
                {
                    buttons[i].targetGraphic.color = buttons[i].colors.pressedColor;
                }
            }
        }
        #endregion
    }
}
