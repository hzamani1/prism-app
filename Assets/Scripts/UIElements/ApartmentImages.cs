﻿using BrydenWoodUnity.DesignData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for setting an image for the apartments of the tower
    /// </summary>
    public class ApartmentImages : MonoBehaviour
    {
        public Dropdown dropdown;
        public Image image;
        // Use this for initialization
        void Start()
        {
            if (dropdown.options.Count > 0)
                image.color = Standards.TaggedObject.ApartmentTypesColours[dropdown.options[dropdown.value].text];
        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Called from the UnityUI when the apartment type dropdown has changed
        /// </summary>
        /// <param name="value">The new value of the dropdown</param>
        public void OnDropDownChanged(int value)
        {
            if (dropdown.options.Count > 0)
                image.color = Standards.TaggedObject.ApartmentTypesColours[dropdown.options[value].text];
        }
    }
}
