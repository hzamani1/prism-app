﻿using BrydenWoodUnity.DesignData;
using ChartAndGraph;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for controlling the Manufacturing Tab
    /// </summary>
    public class ManufacturingTab : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scenes References")]
        public Text title;
        public CanvasPieChart chart;
        public InteractiveScatterPlot plot;
        public ProceduralBuildingManager buildingManager;
        public Text infoText;
        [Header("Assets References")]
        public Material chartMaterial;

        private string shownKey;

        public ManufacturingSystem manufacturingSystem { get; private set; }
        #endregion

        #region Private Fields and Properties
        private int prevValue = 0;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            UpdateInfoDisplay(ManufacturingSystem.Off);
            DisplayControls.previewChanged += OnPreviewModeChanged;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Toggles the display of manufacturing systems
        /// </summary>
        /// <param name="show">On or Off</param>
        public void ShowManufacturingSystem(bool show)
        {
            StartCoroutine(buildingManager.PopulateInterior(show));
        }

        /// <summary>
        /// Called when the manufacturing system selected changes
        /// </summary>
        /// <param name="value">The index of the manufacturing system</param>
        public void OnManufacturingDropdownValueChange(int value)
        {
            StartCoroutine(OnDropDownChanged(value));
        }

        /// <summary>
        /// Called when the preview mode has changed
        /// </summary>
        /// <param name="previewMode">The selected preview mode</param>
        public void OnPreviewModeChanged(PreviewMode previewMode)
        {
            manufacturingSystem = ManufacturingSystem.Off;
            UpdateInfoDisplay(manufacturingSystem);
            buildingManager.ToggleManufacturingSystem(manufacturingSystem);
        }

        /// <summary>
        /// Highlights the volumetric modules of the selected type
        /// </summary>
        /// <param name="args">The arguments from the pie chart</param>
        public void ShowModules(PieChart.PieEventArgs args)
        {
            if (manufacturingSystem == ManufacturingSystem.Panels)
            {
                shownKey = args.Category.Split(' ')[0];
                foreach (var mat in Standards.TaggedObject.panelsMaterials)
                {
                    if (shownKey == mat.Key)
                    {
                        mat.Value.SetColor("_EmissionColor", mat.Value.color * 0.5f);
                    }
                }
                infoText.text = string.Format("{1} panels of {0}m length", Math.Abs(float.Parse(shownKey.Replace("Panel",String.Empty))), args.Value);
            }
            else if (manufacturingSystem == ManufacturingSystem.VolumetricModules)
            {
                shownKey = args.Category.Split(' ')[0];
                foreach (var mat in Standards.TaggedObject.modulesMaterials)
                {
                    if (shownKey == mat.Key)
                    {
                        mat.Value.SetColor("_EmissionColor", mat.Value.color * 0.5f);
                    }
                }
                infoText.text = string.Format("{1} modules of type\r\n{0}\r\n\r\n*for naming convention look at pdf report or site", shownKey.Split(':')[1], args.Value);
            }
        }

        /// <summary>
        /// Hides the highlighted modules
        /// </summary>
        public void HideModules()
        {
            if (manufacturingSystem == ManufacturingSystem.Panels)
            {
                foreach (var mat in Standards.TaggedObject.panelsMaterials)
                {
                    if (shownKey == mat.Key)
                    {
                        mat.Value.SetColor("_EmissionColor", mat.Value.color * 0);
                    }
                }
            }
            else if (manufacturingSystem == ManufacturingSystem.VolumetricModules)
            {
                foreach (var mat in Standards.TaggedObject.modulesMaterials)
                {
                    if (shownKey == mat.Key)
                    {
                        mat.Value.SetColor("_EmissionColor", mat.Value.color * 0);
                    }
                }
            }
            infoText.text = "";
        }

        /// <summary>
        /// Updates the information displayed on the panel
        /// </summary>
        /// <param name="manufacturingSystem">The selected manufacturing system</param>
        public void UpdateInfoDisplay(ManufacturingSystem manufacturingSystem)
        {
            if (buildingManager.previewMode != PreviewMode.Apartments)
            {
                Material m_material;
                switch (manufacturingSystem)
                {
                    case ManufacturingSystem.VolumetricModules:
                        title.text = "Volumetric (Category 1)";
                        var modulesNumbers = buildingManager.GetVerticalModulesNumber();
                        if (modulesNumbers != null)
                        {
                            chart.DataSource.Clear();
                            chart.DataSource.StartBatch();
                            int counter = 0;
                            foreach (var item in modulesNumbers)
                            {
                                Color col;
                                if (Standards.TaggedObject.modulesMaterials.TryGetValue(item.Key, out m_material))
                                {
                                    col = m_material.color;
                                }
                                else
                                {
                                    col = Color.white;
                                }

                                Material typeMat = new Material(chartMaterial);
                                typeMat.color = col;
                                string key = /*"Module" + counter*/item.Key + " (m\xB2)";
                                chart.DataSource.AddCategory(key, typeMat);
                                chart.DataSource.GetMaterial(key).Normal = typeMat;
                                chart.DataSource.GetMaterial(key).Hover = col;
                                chart.DataSource.GetMaterial(key).Selected = col;
                                chart.DataSource.SetValue(key, Math.Round(item.Value));
                                counter++;
                            }
                            chart.DataSource.EndBatch();
                        }
                        var modulesSizes = buildingManager.GetVerticalModulesSizes();
                        if (modulesSizes != null)
                        {
                            List<List<float>> values = new List<List<float>>();
                            List<Color> colors = new List<Color>();
                            foreach (var item in modulesSizes)
                            {
                                //values.Add(item.Value);
                                if (Standards.TaggedObject.modulesMaterials.TryGetValue(item.Key.Split('/')[0], out m_material))
                                {
                                    colors.Add(m_material.color);
                                }
                                else
                                {
                                    colors.Add(Color.white);
                                }
                            }
                            plot.SetValues(modulesSizes, colors, " m width", "modules", "W: ", "L: ");
                            string xAxis = "Module Width (m)";
                            string yAxis = "Module Length (m)";
                            plot.SetAxesDescription(xAxis, yAxis);
                        }
                        break;
                    case ManufacturingSystem.Panels:
                        title.text = "Panelised (Category 2)";
                        var panelsNumbers = buildingManager.GetPanelsNumbers();
                        if (panelsNumbers != null)
                        {
                            chart.DataSource.Clear();
                            chart.DataSource.StartBatch();

                            foreach (var item in panelsNumbers)
                            {
                                Color m_color;
                                if (Standards.TaggedObject.panelsMaterials.TryGetValue(item.Key, out m_material))
                                {
                                    m_color = m_material.color;
                                }
                                else
                                {
                                    m_color = Color.white;
                                }

                                Material typeMat = new Material(chartMaterial);
                                typeMat.color = m_color;

                                chart.DataSource.AddCategory(item.Key, typeMat);
                                chart.DataSource.GetMaterial(item.Key).Normal = typeMat;
                                chart.DataSource.GetMaterial(item.Key).Hover = m_color;
                                chart.DataSource.GetMaterial(item.Key).Selected = m_color;
                                chart.DataSource.SetValue(item.Key, item.Value);
                            }
                            chart.DataSource.EndBatch();
                        }
                        var panelsSizes = buildingManager.GetFacadePanelsSizes();
                        if (panelsSizes != null)
                        {
                            List<List<float>> values = new List<List<float>>();
                            List<Color> colors = new List<Color>();
                            foreach (var item in panelsSizes)
                            {
                                string panelName = item.Key.Split('/')[0];
                                //values.Add(item.Value);
                                Color m_color;
                                if (Standards.TaggedObject.panelsMaterials.TryGetValue(panelName, out m_material))
                                {
                                    m_color = m_material.color;
                                }
                                else
                                {
                                    m_color = Color.white;
                                }
                                colors.Add(m_color);
                            }
                            plot.SetValues(panelsSizes, colors, " m length", "panels", "L: ", "H: ");
                            string xAxis = "Panel Length (m)";
                            string yAxis = "Panel Height (m)";
                            plot.SetAxesDescription(xAxis, yAxis);
                        }
                        break;
                    case ManufacturingSystem.Off:
                        title.text = "System";
                        plot.ResetPlot();
                        chart.DataSource.Clear();
                        break;
                    case ManufacturingSystem.LineLoads:
                        title.text = "Line loads";
                        plot.ResetPlot();
                        chart.DataSource.Clear();
                        break;

                }
            }
        }
        #endregion

        #region Private Methods

        private IEnumerator OnDropDownChanged(int value)
        {
            manufacturingSystem = (ManufacturingSystem)value;
            if (value != 0 && !buildingManager.populateApartments)
            {
                buildingManager.populateApartments = true;
                yield return StartCoroutine(buildingManager.UpdateInternalLayout());
            }
            prevValue = value;
            buildingManager.ToggleManufacturingSystem(manufacturingSystem);
            UpdateInfoDisplay(manufacturingSystem);
        }

        #endregion
    }
}
