﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for setting up the UI ribbon
    /// </summary>
    public class RibbonSetup : MonoBehaviour
    {
        #region Public Fields and Properties
        public GameObject popupPrefab;
        public GameObject slidingPrefab;
        public Sprite ColorTemplateImage;
        public List<GameObject> current_tabs;
        public List<Color32> tab_colors = new List<Color32>();
        public Color32 tab_Color;
        public Color32[] temp_centroids;
        public List<TabTypes> tabs;
        [HideInInspector]
        public Color32[] centroids;
        #endregion

        #region Private Fields and Properties
        private Vector2 SpriteDimensions;
        private int sizeX;
        private int sizeY;
        private Texture2D myTexture;
        private List<Color32>[] AllClusters;
        private List<float> distances = new List<float>();
        private int r_value, g_value, b_value = 0;
        private int clusteringcounter = 0;
        private int dominanceCounter = 0;
        private int recursion = 0;
        private int ribbonSpace = 32;
        private Vector3 start_tab_position;
        private List<Vector3> tabsVectors = new List<Vector3>();
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Vector3 tab_pos = transform.GetChild(i).position;
                tabsVectors.Add(tab_pos);
                Debug.Log(tab_pos.ToString());
            }
            start_tab_position = new Vector3(ribbonSpace, 0, 0);
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Populates the ribbon with a given number of tabs
        /// </summary>
        /// <param name="tabs">The total number of tabs to be populated</param>
        public void PopulateRibbon(int tabs)
        {
            if (current_tabs == null)
            {
                current_tabs = new List<GameObject>();
            }
            for (int i = 0; i < tabs; i++)
            {

                GameObject new_tab = Instantiate(popupPrefab);
                new_tab.name = "tab_" + i.ToString();
                new_tab.transform.SetParent(transform);
                new_tab.transform.localScale = new Vector3(1, 1, 1);
                current_tabs.Add(new_tab);
                Color32 current_col = new_tab.transform.GetChild(0).GetComponent<Image>().color;
                tab_colors.Add(current_col);
            }
            Debug.Log(current_tabs.Count.ToString());
        }

        /// <summary>
        /// Populates the ribbon
        /// </summary>
        public void PopulateRibbon()
        {
            if (current_tabs == null)
            {
                current_tabs = new List<GameObject>();
            }
            for (int i = 0; i < tabs.Count; i++)
            {
                switch (tabs[i])
                {
                    case TabTypes.PopUp:
                        GameObject new_tab = Instantiate(popupPrefab);
                        new_tab.name = "tab_" + i.ToString();
                        new_tab.transform.SetParent(transform);
                        new_tab.transform.localScale = new Vector3(1, 1, 1);
                        current_tabs.Add(new_tab);
                        Color32 current_col = new_tab.transform.GetChild(0).GetComponent<Image>().color;
                        tab_colors.Add(current_col);

                        break;

                    case TabTypes.Sliding:
                        GameObject m_tab = Instantiate(slidingPrefab);
                        m_tab.name = "tab_" + i.ToString();
                        m_tab.transform.SetParent(transform);
                        m_tab.transform.localScale = new Vector3(1, 1, 1);
                        current_tabs.Add(m_tab);
                        Color32 current_col2 = m_tab.transform.GetChild(0).GetComponent<Image>().color;
                        tab_colors.Add(current_col2);

                        break;
                }
            }
            Debug.Log(current_tabs.Count.ToString());
        }

        /// <summary>
        /// Adds an item to the ribbon
        /// </summary>
        public void AddOneRibbonItem()
        {
            if (current_tabs == null)
            {
                current_tabs = new List<GameObject>();
            }
            GameObject new_tab = Instantiate(popupPrefab);
            new_tab.name = "tab_" + (current_tabs.Count).ToString();
            new_tab.transform.SetParent(transform);
            new_tab.transform.localScale = new Vector3(1, 1, 1);
            current_tabs.Add(new_tab);
            Color32 current_col = new_tab.transform.GetChild(0).GetComponent<Image>().color;
            tab_colors.Add(current_col);
            Debug.Log(current_tabs.Count.ToString());
        }

        /// <summary>
        /// Adds an item to the ribbon with a given type
        /// </summary>
        /// <param name="type"></param>
        public void AddOneRibbonItem(TabTypes type)
        {
            if (current_tabs == null)
            {
                current_tabs = new List<GameObject>();
            }
            switch (type)
            {
                case TabTypes.PopUp:
                    GameObject new_tab = Instantiate(popupPrefab);
                    new_tab.name = "tab_";
                    new_tab.transform.SetParent(transform);
                    new_tab.transform.localScale = new Vector3(1, 1, 1);
                    current_tabs.Add(new_tab);
                    Color32 current_col = new_tab.transform.GetChild(0).GetComponent<Image>().color;
                    tab_colors.Add(current_col);

                    break;

                case TabTypes.Sliding:
                    GameObject m_tab = Instantiate(slidingPrefab);
                    m_tab.name = "tab_";
                    m_tab.transform.SetParent(transform);
                    m_tab.transform.localScale = new Vector3(1, 1, 1);
                    current_tabs.Add(m_tab);
                    Color32 current_col2 = m_tab.transform.GetChild(0).GetComponent<Image>().color;
                    tab_colors.Add(current_col2);

                    break;
            }
        }

        /// <summary>
        /// Sets the colours of a tab
        /// </summary>
        /// <param name="my_col">The new color</param>
        /// <param name="thisTab">The tab to be coloured</param>
        public void ColorizeTabSimple(Color32 my_col, GameObject thisTab)
        {
            thisTab.transform.GetChild(0).GetComponent<Image>().color = my_col;
        }

        /// <summary>
        /// Gets the dominant colours from an image
        /// </summary>
        /// <param name="color_num">The number of colours to get</param>
        public void DominantColors(int color_num)
        {
            SpriteDimensions = ColorTemplateImage.rect.size;
            sizeX = (int)SpriteDimensions.x;
            sizeY = (int)SpriteDimensions.y;
            Debug.Log("width: " + SpriteDimensions.x + ",  " + "height: " + SpriteDimensions.y);
            myTexture = ColorTemplateImage.texture;
            centroids = new Color32[color_num];
            temp_centroids = new Color32[color_num];
            AllClusters = new List<Color32>[color_num];
            for (int i = 0; i < color_num; i++)
            {
                Color32 randomColor = myTexture.GetPixel((int)UnityEngine.Random.Range(0, sizeX - 1), (int)UnityEngine.Random.Range(0, sizeY - 1));
                List<Color32> cluster = new List<Color32>();
                centroids[i] = randomColor;
                AllClusters[i] = cluster;
            }
            ClusteringRecursion(color_num, centroids);
        }

        /// <summary>
        /// Recursive clustering methods
        /// </summary>
        /// <param name="color_num">The number of colours to get</param>
        /// <param name="centers">The total number of colours given</param>
        public void ClusteringRecursion(int color_num, Color32[] centers)
        {
            recursion++;
            Debug.Log(recursion.ToString());

            Color32[] _tempcentroids = new Color32[color_num];
            for (int j = 0; j < sizeX; j++)
            {
                for (int k = 0; k < sizeY; k++)
                {
                    for (int c = 0; c < color_num; c++)
                    {
                        Color32 currentPixel = myTexture.GetPixel(j, k);
                        float distance = RGBdistance(centers[c], currentPixel);
                        distances.Add(distance);
                        if (c == color_num - 1)
                        {
                            float minDistance = distances.Min();
                            //int minIndex = distances.ToList().IndexOf(minDistance);
                            int minIndex = distances.IndexOf(minDistance);
                            AllClusters[minIndex].Add(currentPixel);
                            //Array.Clear(distances, 0, distances.Length);
                            distances.Clear();
                        }
                    }
                }
            }

            for (int l = 0; l < color_num; l++)
            {

                r_value = g_value = b_value = 0;
                int clusterSize = AllClusters[l].Count;

                if (clusterSize != 0)
                {
                    for (int m = 0; m < clusterSize; m++)
                    {
                        r_value += AllClusters[l][m].r;
                        g_value += AllClusters[l][m].g;
                        b_value += AllClusters[l][m].b;
                    }
                    //Debug.Log("R: "+(r_value/clusterSize).ToString());
                    Color32 new_centroid = new Color32((byte)(r_value / clusterSize), (byte)(g_value / clusterSize), (byte)(b_value / clusterSize), 255);
                    _tempcentroids[l] = new_centroid;
                }
                else
                {
                    Color32 new_centroid = myTexture.GetPixel((int)UnityEngine.Random.Range(0, sizeX - 1), (int)UnityEngine.Random.Range(0, sizeY - 1));
                    _tempcentroids[l] = new_centroid;
                }


            }

            int _dominanceCounter = 0;

            for (int g = 0; g < color_num; g++)
            {
                if (_tempcentroids[g].r == centers[g].r && _tempcentroids[g].g == centers[g].g && _tempcentroids[g].b == centers[g].b)

                {
                    _dominanceCounter++;
                }
            }
            if (_dominanceCounter == color_num)
            {
                Debug.Log("Dominant Colors Found!!");
                temp_centroids = _tempcentroids;
            }
            if (_dominanceCounter != color_num)
            {
                ClusteringRecursion(color_num, _tempcentroids);
            }

        }

        /// <summary>
        /// Euclidean distance between colours
        /// </summary>
        /// <param name="a">First colour</param>
        /// <param name="b">Second colour</param>
        /// <returns></returns>
        public float RGBdistance(Color32 a, Color32 b)
        {
            float distance = Mathf.Sqrt(Mathf.Pow((a.r - b.r), 2) + Mathf.Pow((a.g - b.g), 2) + Mathf.Pow((a.b - b.b), 2));
            return distance;
        }

        /// <summary>
        /// Toggles 
        /// </summary>
        /// <param name="ready">On or Off</param>
        public void ReadyToPlay(bool ready)
        {
            transform.GetComponent<HorizontalLayoutGroup>().enabled = ready;
        }
        #endregion
    }
}
