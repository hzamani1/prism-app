﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.DesignData;
using System.Collections;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component which manages the Apartment Layout modifications
    /// </summary>
    public class AptLayoutModifications: MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Button toggleBalconiesButon;
        public ProceduralBuildingManager buildingManager;
        public Text refreshNotification;
        #endregion

        #region Private Fields and Properties
        private BaseDesignData selectedGeometry;
        private bool hasBalcony;
        #endregion

        #region MonoBehaviour Methods
        void Start()
        {
            BaseDesignData.selected += OnSelect;
            Selector.deselect += OnDeselect;
        }

        private void OnDestroy()
        {
            BaseDesignData.selected -= OnSelect;
            Selector.deselect -= OnDeselect;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when a Design Data object is selected
        /// </summary>
        /// <param name="designData">The selected Design Data object</param>
        public void OnSelect(BaseDesignData designData)
        {
            selectedGeometry = designData;
            var room = designData as RoomUnity;
            if (room != null)
            {
                if (buildingManager.exteriorBalconies == Balconies.Inset)
                {
                    if (room.name == "LRR" || room.name == "LR")
                    {
                        toggleBalconiesButon.interactable = true;
                    }
                }
                else
                {
                    if (room.name == "DB" ||
                        room.name == "DBB" ||
                        room.name == "PB" ||
                        room.name == "PBB" ||
                        room.name == "LR" ||
                        room.name == "LRR" ||
                        room.name == "PSB")
                    {
                        toggleBalconiesButon.interactable = true;
                    }
                }
            }
            else
            {
                toggleBalconiesButon.interactable = false;
            }

            if (refreshNotification.gameObject.activeSelf)
            {
                refreshNotification.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Called when the Design Data object is deselected
        /// </summary>
        public void OnDeselect()
        {
            selectedGeometry = null;
            toggleBalconiesButon.interactable = false;
        }
        #endregion
    }
}
