﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing the changes in value of a Dropdown
    /// </summary>
    public class CancelableDropdown : MonoBehaviour, ICancelable
    {
        
        public OnDropdownSubmitted valueSubmitted;
        [Tooltip("Whether it should call for refreshing the whole project or just the selected building")]
        public bool allProject = true;

        private Dropdown dropdown;
        private string prevValue;

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            dropdown = GetComponent<Dropdown>();
            prevValue = dropdown.value.ToString();
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the value of the Dropdown changes
        /// </summary>
        /// <param name="val">The new value</param>
        public void OnValueChanged(int val)
        {
            if (allProject)
            {
                RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevValue, val.ToString() });
            }
            else
            {
                RefreshBuildingPopUp.TaggedObject.AddChangedValue(this, new string[] { prevValue, val.ToString() });
            }
        }

        /// <summary>
        /// Resets the value of the Dropdown
        /// </summary>
        /// <param name="prevValue">The previous value</param>
        public void ResetValue(string prevValue)
        {
            dropdown.SetValue(int.Parse(prevValue));
            prevValue = dropdown.value.ToString();
        }

        /// <summary>
        /// Submits the new value of the dropdown
        /// </summary>
        /// <param name="currentValue">The new value</param>
        public void SubmitValue(string currentValue)
        {
            valueSubmitted.Invoke(int.Parse(currentValue));
            prevValue = dropdown.value.ToString();
        }
        #endregion
    }

    /// <summary>
    /// A UnityEvent class for when the new values are submitted
    /// </summary>
    [System.Serializable]
    public class OnDropdownSubmitted : UnityEvent<int>
    {

    }
}
