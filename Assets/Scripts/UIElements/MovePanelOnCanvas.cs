﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for moving UI elements on Canvas between two fixed positions
    /// </summary>
    public class MovePanelOnCanvas : MonoBehaviour
    {
        #region Public Fields and Properties
        public Vector3 onPosition;
        public Vector3 offPosition;
        public bool inCoroutine = false;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Toggles the position of the UI element
        /// </summary>
        /// <param name="show">On or Off</param>
        public void OnToggle(bool show)
        {
            if (inCoroutine) { }
            else
            {
                if (show)
                {
                    GetComponent<RectTransform>().anchoredPosition = onPosition;
                }
                else
                {
                    GetComponent<RectTransform>().anchoredPosition = offPosition;
                }
            }
        }
        #endregion
    }
}
