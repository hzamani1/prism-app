﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for a Building UI Element
    /// </summary>
    public class BuildingElement : MonoBehaviour, ICancelable
    {
        #region Public Fields and Properties
        public string buildingName;
        public int exteriorBalconies;
        public int coreAllignment;
        public int typology;
        public float totalHeight;
        public BuildingType buildingType;
        public ProceduralBuilding proceduralBuilding;
        public Dropdown coreAlignmentDropdown;
        public Dropdown typologyDropdown;
        public InputField buildingNameInputField;
        public InputField corridorWidthField;
        public Button delete;
        public Image image;
        public Sprite[] sprites;

        public BuildingData buildingData;
        #endregion

        private string prevTypology;
        private string prevCoreAlignment;
        private float originalHeight;
        public float currentHeight;

        #region Events
        /// <summary>
        /// Used when the element is selected
        /// </summary>
        /// <param name="sender">The selected element</param>
        public delegate void OnSelected(BuildingElement sender);
        /// <summary>
        /// Triggered when the element is selected
        /// </summary>
        public static event OnSelected selected;
        /// <summary>
        /// Triggered when the element has been deleted
        /// </summary>
        public static event OnSelected deleted;
        /// <summary>
        /// Triggered when the element has changed
        /// </summary>
        public static event OnSelected changed;
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
        }

        // Use this for initialization
        void Start()
        {
            delete.onClick.AddListener(OnDeletePressed);
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called from the UnityUI when an element is deleted
        /// </summary>
        public void OnDeletePressed()
        {
            GameObject.Find("RemoveBuildingPanel").GetComponent<MovePanelOnCanvas>().OnToggle(true);
            GameObject.Find("SubmitRemoveButton").GetComponent<Button>().onClick.RemoveAllListeners();
            GameObject.Find("SubmitRemoveButton").GetComponent<Button>().onClick.AddListener(OnDelete);
        }

        /// <summary>
        /// Initializes the instance
        /// </summary>
        public void Initialize()
        {
            buildingData = new BuildingData();
            prevTypology = "typology;" + 0;
            prevCoreAlignment = "coreAlignment;" + 0;
            originalHeight = GetComponent<RectTransform>().sizeDelta.y;
            SendMessageUpwards("UpdateChildrenSize");
            GetComponent<Image>().color = GetComponent<Toggle>().colors.normalColor;
            GetComponent<RectTransform>().sizeDelta = new Vector2(42, currentHeight);
            image.sprite = sprites[0];
        }

        /// <summary>
        /// Initializes the instance
        /// </summary>
        /// <param name="aptDepth">The apartment depth</param>
        /// <param name="crdrWidth">The corridor width</param>
        /// <param name="type">The type of the building</param>
        public void Initialize(float aptDepth, float crdrWidth, BuildingType type)
        {
            Initialize();
            buildingData.apartmentDepth = aptDepth;
            buildingData.corridorWidth = crdrWidth;
            buildingType = type;
            buildingData.buildingType = (int)type;
            prevTypology = "typology;" + 0;
            prevCoreAlignment = "coreAlignment;" + 0;
            GetComponent<Image>().color = GetComponent<Toggle>().colors.normalColor;
            GetComponent<RectTransform>().sizeDelta = new Vector2(42, currentHeight);
            image.sprite = sprites[0];
        }

        /// <summary>
        /// Called when the building element is deselected
        /// </summary>
        public void Deselect()
        {
            GetComponent<Image>().color = GetComponent<Toggle>().colors.pressedColor;
            GetComponent<Toggle>().SetValue(false);
            GetComponent<RectTransform>().sizeDelta = new Vector2(40, currentHeight);
            if (currentHeight<60)
            {
                delete.gameObject.SetActive(false);
            }
            image.sprite = sprites[1];
        }

        /// <summary>
        /// Sets the Building Data of the element
        /// </summary>
        /// <param name="building">The new building data</param>
        public void SetBuildingData(BuildingData building)
        {
            buildingData = building;
            typology = building.linearTypology;
            coreAllignment = building.coreAllignment;
            exteriorBalconies = building.exteriorBalconies;
            buildingName = building.name;
            buildingType = (BuildingType)building.buildingType;

            if (corridorWidthField != null)
                corridorWidthField.SetValue(building.corridorWidth.ToString("0.00"));
            if (typologyDropdown != null)
                typologyDropdown.SetValue(building.linearTypology);
            if (coreAlignmentDropdown != null)
                coreAlignmentDropdown.SetValue(building.coreAllignment);
            buildingNameInputField.SetValue(building.name);

            proceduralBuilding.coreAllignment = (CoreAllignment)coreAllignment;
            proceduralBuilding.typology = (LinearTypology)typology;
            proceduralBuilding.buildingName = building.name;
            proceduralBuilding.corridorWidth = building.corridorWidth;
            proceduralBuilding.apartmentDepth = building.apartmentDepth;
            proceduralBuilding.buildingType = (BuildingType)building.buildingType;

            prevTypology = "typology;" + building.linearTypology;
            prevCoreAlignment = "coreAlignment;" + building.coreAllignment;

            UpdateHeightsPerLevel();
        }

        /// <summary>
        /// Sets the procedural building which corresponds to this element
        /// </summary>
        /// <param name="build">The new procedural building</param>
        public void SetProceduralBuilding(ProceduralBuilding build)
        {
            proceduralBuilding = build;
            proceduralBuilding.buildingType = buildingType;
            proceduralBuilding.corridorWidth = buildingData.corridorWidth;
            proceduralBuilding.apartmentDepth = buildingData.apartmentDepth;
            build.buildingElement = this;
            prevTypology = "typology;" + (int)build.typology;
            prevCoreAlignment = "coreAlignment;" + (int)build.coreAllignment;
            proceduralBuilding.Select();
        }

        /// <summary>
        /// Returns the Y value for a given floor index
        /// </summary>
        /// <param name="floorIndex">The floor index</param>
        /// <returns>Float</returns>
        public float GetYForLevel(int floorIndex)
        {
            int levelIndex = buildingData.levels[floorIndex][0];
            return GetYForLevel(levelIndex, floorIndex);
        }

        /// <summary>
        /// Returns the Y value of the current floor
        /// </summary>
        /// <returns>Float</returns>
        public float GetYForLevel()
        {
            int levelIndex = buildingData.levels.Last()[0];
            return GetYForLevel(levelIndex, buildingData.levels.Count - 1);
        }

        /// <summary>
        /// Returns the Y value for a given floor index and level index
        /// </summary>
        /// <param name="levelIndex">The level index</param>
        /// <param name="floorIndex">The floor index/param>
        /// <returns>Float</returns>
        public float GetYForLevel(int levelIndex, int floorIndex)
        {
            UpdateHeightsPerLevel();
            var yValue = 0.0f;

            if (buildingData.heightPerLevel != null)
            {
                for (int i = 0; i < levelIndex; i++)
                {
                    yValue += buildingData.heightPerLevel[i];
                }
            }

            return yValue;
        }

        /// <summary>
        /// Called when a floor is deleted
        /// </summary>
        /// <param name="floorIndex">The index of the floor to be deleted</param>
        public void OnFloorDeleted(int floorIndex)
        {
            if (floorIndex < buildingData.levels.Count)
                buildingData.levels.RemoveAt(floorIndex);
            if (floorIndex < buildingData.percentages.Count)
                buildingData.percentages.RemoveAt(floorIndex);
            if (buildingType == BuildingType.Tower && floorIndex < buildingData.aptTypesTower.Count)
            {
                buildingData.aptTypesTower.RemoveAt(floorIndex);
            }
            proceduralBuilding.DeleteFloor(floorIndex);
            UpdateHeightsPerLevel();
        }

        /// <summary>
        /// Called when a floor is added
        /// </summary>
        /// <param name="element">The floor element to be added</param>
        /// <param name="floorIndex">The index of the floor element to be added</param>
        /// <param name="aptTypes">The list of apartment types of the floor to be added</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor OnFloorAdded(FloorElement element, int floorIndex, string[] aptTypes = null)
        {
            buildingData.levels.Add(element.levels);
            buildingData.floorHeights.Add(element.floorHeight);
            if (buildingType == BuildingType.Tower)
            {
                buildingData.aptTypesTower.Add(aptTypes);
                buildingData.percentages.Add(FloorElement.GetTowerPercentages(aptTypes));
            }
            return proceduralBuilding.AddFloor(floorIndex, "Floor");
        }

        /// <summary>
        /// Called when the values of a floor element have been updated
        /// </summary>
        /// <param name="floorIndex">The index of the updated floor element</param>
        /// <param name="levels">The new levels of the element</param>
        /// <param name="f2f">The new floor to floor height of the element</param>
        /// <param name="aptTypes">The new list of apartment types of the element</param>
        public void UpdateFloorValues(int floorIndex, List<int> levels, float f2f, string[] aptTypes = null)
        {
            buildingData.levels[floorIndex] = levels;
            buildingData.floorHeights[floorIndex] = f2f;
            if (buildingType == BuildingType.Tower)
            {
                buildingData.aptTypesTower[floorIndex] = aptTypes;
                buildingData.percentages[floorIndex] = FloorElement.GetTowerPercentages(aptTypes);
            }
            UpdateHeightsPerLevel();
        }

        /// <summary>
        /// Recalculates the height (Y-values) per level of each floor
        /// </summary>
        public void UpdateHeightsPerLevel()
        {
            try
            {
                buildingData.heightPerLevel = new List<float>();
                for (int i = 0; i < buildingData.levels.Count; i++)
                {
                    bool issue = false;
                    if (buildingData.buildingType == 0 && buildingData.percentages.Count == buildingData.levels.Count && i < buildingData.levels.Count - 1)
                    {
                        if (buildingData.percentages[i].Count != buildingData.percentages[i + 1].Count)
                        {
                            issue = true;
                        }
                        else
                        {
                            foreach (var item in buildingData.percentages[i])
                            {
                                if (!buildingData.percentages[i + 1].ContainsKey(item.Key))
                                {
                                    issue = true;
                                }
                                else
                                {
                                    if (buildingData.percentages[i + 1][item.Key] != item.Value)
                                    {
                                        issue = true;
                                    }
                                }
                            }
                        }
                    }
                    else if (buildingData.buildingType == 1 && i < buildingData.levels.Count - 1)
                    {
                        if (buildingData.aptTypesTower[i] != buildingData.aptTypesTower[i + 1])
                        {
                            issue = true;
                        }
                    }

                    for (int j = 0; j < buildingData.levels[i].Count; j++)
                    {
                        float addOn = 0.0f;
                        if (j == buildingData.levels[i].Count - 1 && issue)
                        {
                            addOn = 0.5f;
                        }
                        buildingData.heightPerLevel.Add(buildingData.floorHeights[i] + addOn);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        /// <summary>
        /// Called when the element is deleted
        /// </summary>
        public void OnDelete()
        {
            GameObject.Find("SubmitRemoveButton").GetComponent<Button>().onClick.RemoveListener(OnDelete);
            GameObject.Find("RemoveBuildingPanel").GetComponent<MovePanelOnCanvas>().OnToggle(false);
            proceduralBuilding.Delete();
            buildingData = null;
            if (deleted != null)
            {
                deleted(this);
            }
            SendMessageUpwards("BuildingChildDeleted", SendMessageOptions.DontRequireReceiver);
        }

        /// <summary>
        /// Called when the element is selected
        /// </summary>
        /// <param name="isSelected">Toggles selection</param>
        public void Selected(bool isSelected)
        {
            currentHeight = GetComponent<RectTransform>().sizeDelta.y;
            GetComponent<Image>().color = GetComponent<Toggle>().colors.normalColor;
            GetComponent<RectTransform>().sizeDelta = new Vector2(42, originalHeight);
            image.sprite = sprites[0];
            if (selected != null)
            {
                selected(this);
            }
            proceduralBuilding.Select();
            delete.gameObject.SetActive(true);
        }

        /// <summary>
        /// Called by the Unity UI when the corridor width has changed
        /// </summary>
        /// <param name="value">The new corridor width</param>
        public void OnCorridorWidthValueChanged(string value)
        {
            float width = 1.2f;
            if (float.TryParse(value, out width))
            {
                buildingData.corridorWidth = width;
                proceduralBuilding.corridorWidth = width;
            }
        }

        /// <summary>
        /// Called by the Unity UI when the name of the building has changed
        /// </summary>
        /// <param name="name">The new name of the building</param>
        public void OnBuildingNameChanged(string name)
        {
            buildingName = name;
            buildingData.name = name;
            proceduralBuilding.buildingName = name;
            proceduralBuilding.gameObject.name = name;
        }

        /// <summary>
        /// Called when the Core Allignment of the building changes
        /// </summary>
        /// <param name="value">The new core allignment</param>
        public void OnCoreAllignmentChanged(int value)
        {
            RefreshBuildingPopUp.TaggedObject.AddChangedValue(this, new string[] { prevCoreAlignment, "coreAlignment;" + value.ToString() });
        }

        /// <summary>
        /// Called when the aspect of the building changes
        /// </summary>
        /// <param name="value">The new aspect</param>
        public void OnAspectChanged(int value)
        {
            RefreshBuildingPopUp.TaggedObject.AddChangedValue(this, new string[] { prevTypology, "typology;" + value.ToString() });
        }

        /// <summary>
        /// Resets the values of the building element
        /// </summary>
        /// <param name="prevValue">The previous values</param>
        public virtual void ResetValue(string prevValue)
        {
            var cells = prevValue.Split(';');
            switch (cells[0])
            {
                case "coreAlignment":
                    int coreValue = int.Parse(cells[1]);
                    if (coreAlignmentDropdown != null)
                        coreAlignmentDropdown.SetValue(coreValue);
                    prevCoreAlignment = "coreAlignment;" + coreValue;
                    break;
                case "typology":
                    typology = int.Parse(cells[1]);
                    if (typologyDropdown != null)
                        typologyDropdown.SetValue(typology);
                    prevTypology = "typology;" + typology;
                    break;
            }
        }

        /// <summary>
        /// Submits the new values for the building element
        /// </summary>
        /// <param name="currentValue">The new values</param>
        public virtual void SubmitValue(string currentValue)
        {
            var cells = currentValue.Split(';');
            switch (cells[0])
            {
                case "coreAlignment":
                    int coreValue = int.Parse(cells[1]);
                    if (typology != 0)
                    {
                        coreAllignment = coreValue + 1;
                    }
                    else
                    {
                        coreAllignment = coreValue;
                    }
                    proceduralBuilding.coreAllignment = (CoreAllignment)coreAllignment;
                    buildingData.coreAllignment = coreAllignment;
                    if (changed != null)
                    {
                        changed(this);
                    }
                    prevCoreAlignment = "coreAlignment;" + coreValue;
                    break;
                case "typology":
                    typology = int.Parse(cells[1]);
                    proceduralBuilding.typology = (LinearTypology)typology;
                    buildingData.linearTypology = typology;
                    try
                    {
                        if (typology != 0 && coreAlignmentDropdown.options[0].text == "Centre")
                        {
                            coreAlignmentDropdown.options.RemoveAt(0);
                            coreAlignmentDropdown.value = 0;
                            buildingData.coreAllignment = 1;
                            proceduralBuilding.coreAllignment = CoreAllignment.Left;
                            coreAlignmentDropdown.RefreshShownValue();
                        }
                        else if (typology == 0 && coreAlignmentDropdown.options.Count == 2)
                        {
                            coreAlignmentDropdown.options.Insert(0, new Dropdown.OptionData("Centre"));
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e.ToString());
                    }
                    if (changed != null)
                    {
                        changed(this);
                    }
                    prevTypology = "typology;" + typology;
                    break;
            }
        }
        #endregion
    }
}
