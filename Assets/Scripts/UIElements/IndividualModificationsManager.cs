﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.DesignData;
using System;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for  managing the modifications of individual design data entities
    /// </summary>
    public class IndividualModificationsManager : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References")]
        public Dropdown typeDropdown;
        public Text typeText;
        public Button swapButton;
        public Button combineButton;
        public Button resolveToAll;
        public Button resolveToNext;
        public Button undoCombineButton;
        public Toggle flipInterior;

        public ProceduralBuildingManager buildingManager;
        #endregion

        private BaseDesignData selectedGeometry;

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            BaseDesignData.selected += OnSelect;
            Selector.deselect += OnDeselect;
            typeDropdown.ClearOptions();
            var types = Standards.TaggedObject.ApartmentTypesMinimumSizes;
            foreach (var item in types)
            {
                if (item.Key != "Commercial" && item.Key!= "Other")
                {
                    if (item.Key == "SpecialApt")
                    {
                        typeDropdown.options.Add(new Dropdown.OptionData("Unresolved"));
                    }
                    else
                    {
                        typeDropdown.options.Add(new Dropdown.OptionData(item.Key));
                    }
                }
            }
            typeDropdown.RefreshShownValue();
            typeDropdown.onValueChanged.AddListener(ChangeType);
            typeDropdown.interactable = false;
            swapButton.interactable = false;
            combineButton.interactable = false;
            resolveToAll.interactable = false;
            resolveToNext.interactable = false;
            //undoCombineButton.interactable = false;
            flipInterior.interactable = false;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            BaseDesignData.selected -= OnSelect;
            Selector.deselect -= OnDeselect;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called from the Unity UI to flip the interior of an apartment
        /// </summary>
        /// <param name="flip">Whether to flip the interior</param>
        public void OnFlipInterior(bool flip)
        {
            var apt = selectedGeometry as ApartmentUnity;
            apt.FlipInterior(flip);
        }

        /// <summary>
        /// Called from the Unity UI when the Resolve All button is pressed
        /// </summary>
        public void OnResolveToAll()
        {
            var apt = selectedGeometry as ApartmentUnity;
            if (apt != null)
            {
                StartCoroutine(apt.ResolveWithAll());
            }
        }

        /// <summary>
        /// Called from the Unity UI to change the type of the selected apartments
        /// </summary>
        /// <param name="value">The index of the selected type</param>
        public void ChangeType(int value)
        {
            var selectedGeoms = Selector.TaggedObject.selected;
            if (selectedGeoms != null && selectedGeoms.Count > 0)
            {
                for (int i = 0; i < selectedGeoms.Count; i++)
                {
                    var apt = (ApartmentUnity)selectedGeoms[i];
                    if (apt != null)
                    {
                        string type = typeDropdown.options[value].text;
                        apt.OnApartmentTypeChanged((type == "Unresolved") ? "SpecialApt" : type);
                    }
                }
            }
        }

        /// <summary>
        /// Called from the Unity UI when the resolve to Next button is pressed
        /// </summary>
        public void OnResolveToNext()
        {
            var apt = selectedGeometry as ApartmentUnity;
            if (apt != null)
            {
                apt.ResolveWithNext();
            }
        }

        /// <summary>
        /// Called when a design data entity is selected
        /// </summary>
        /// <param name="designData">The selected entity</param>
        public void OnSelect(BaseDesignData designData)
        {
            selectedGeometry = designData;
            var infos = designData.GetInfoText();
            swapButton.interactable = true;
            typeDropdown.interactable = true;
            //combineButton.interactable = true;
            //undoCombineButton.interactable = true;
            flipInterior.interactable = true;
            try
            {
                var apt = selectedGeometry as ApartmentUnity;

                if (apt != null)
                {
                    typeDropdown.interactable = true;
                    flipInterior.interactable = true;
                    flipInterior.SetValue(apt.flipped);
                    for (int i = 0; i < typeDropdown.options.Count; i++)
                    {
                        if (typeDropdown.options[i].text == apt.ApartmentType)
                        {
                            typeDropdown.SetValue(i);
                        }
                    }
                    if (apt.ApartmentType == "SpecialApt")
                    {
                        resolveToAll.interactable = true;
                        resolveToNext.interactable = true;
                    }
                    else
                    {
                        resolveToAll.interactable = false;
                        resolveToNext.interactable = false;
                    }
                }
                else
                {
                    var room = selectedGeometry as RoomUnity;

                    typeDropdown.interactable = false;
                    swapButton.interactable = false;
                    combineButton.interactable = false;
                    resolveToAll.interactable = false;
                    resolveToNext.interactable = false;
                    //undoCombineButton.interactable = false;
                    flipInterior.interactable = false;
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        /// <summary>
        /// Called when a design data entity is deselected
        /// </summary>
        public void OnDeselect()
        {
            typeDropdown.interactable = false;
            swapButton.interactable = false;
            combineButton.interactable = false;
            resolveToAll.interactable = false;
            resolveToNext.interactable = false;
            //undoCombineButton.interactable = false;
            flipInterior.interactable = false;
        }
        #endregion
    }
}
