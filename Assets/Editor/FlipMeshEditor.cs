﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// A custom editor script for flipping meshes
/// </summary>
[CustomEditor(typeof(FlipMesh))]
public class FlipMeshEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Flip Normals"))
        {
            FlipMesh myTarget = (FlipMesh)target;
            myTarget.FlipSharedMesh();
        }
    }
}
